package com.redapplemart.rukula.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.fragments.ChangeBankAccConfirmFragment;
import com.redapplemart.rukula.fragments.ChangePinFragment;
import com.redapplemart.rukula.fragments.CreatePinFragment;
import com.redapplemart.rukula.fragments.HelpFragment;
import com.redapplemart.rukula.fragments.LoginFragment;
import com.redapplemart.rukula.fragments.PinEnterFragment;
import com.redapplemart.rukula.fragments.RegisterBankAccFragment;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Merchant;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener
        , RetrofitResponseInterface<Merchant> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;
    private static final int PERMISSION_REQUEST_FILE_PERMISSION_ID = 1;

    private static final int RETROFIT_REQUEST_USER_LOGIN = 0;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    //last viewed fragment
    private Fragment mLastViewedFragment;

    //app drawer
    private DrawerLayout mDrawerLayout;
    //base fragment container
    private FrameLayout mFragContainer;

    private FrameLayout mChangePinContainerFL, mChangeBankContainerFL, mHelpContainerFL, mLogoutContainerFL;
    private TextView mMerchantNameTxt, mMerchantIDTxt;
    private LinearLayout mMerchantDataContainer;
    private Space mSpace1, mSpace2;

    private Toolbar mToolBar;

    //current logged in merchant's details
    private Merchant mCurrentMerchantDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mFragContainer = findViewById(R.id.content_home_fr_lay_base);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolBar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mSessionManager = SessionManager.getInstance(this);
        mProgressDialog = Utility.createProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ImageView img_close = findViewById(R.id.nav_header_img_close);
        img_close.setOnClickListener(this);

        mChangePinContainerFL = findViewById(R.id.act_home_fr_lay_change_pin);
        mChangeBankContainerFL = findViewById(R.id.act_home_fr_lay_change_bank);
        mHelpContainerFL = findViewById(R.id.act_home_fr_lay_help);
        mLogoutContainerFL = findViewById(R.id.act_home_fr_lay_logout);

        mMerchantDataContainer = findViewById(R.id.act_home_li_lay_user_data_container);

        mMerchantNameTxt = findViewById(R.id.act_home_txt_merch_name);
        mMerchantIDTxt = findViewById(R.id.act_home_txt_id);

        mSpace1 = findViewById(R.id.act_home_space_1);
        mSpace2 = findViewById(R.id.act_home_space_2);

        //drawer items
        ImageView back_to_login = findViewById(R.id.nav_head_back_to_login);
        TextView chn_pin = findViewById(R.id.act_home_txt_change_pin);
        TextView chn_bank = findViewById(R.id.act_home_txt_change_bank);
        TextView help = findViewById(R.id.act_home_txt_help);
        TextView logout = findViewById(R.id.act_home_txt_logout);
        //TODO:: if login then redirect to home else do not do anything
        //back_to_login.setOnClickListener(this); //ashain: this pushed to login, which is wrong, follow standard logout procedure
        chn_pin.setOnClickListener(this);
        chn_bank.setOnClickListener(this);
        help.setOnClickListener(this);
        logout.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            mCurrentMerchantDetails = null;

            hideOrShowVerifiedFunctions(false);

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.INTERNET)) {
                    Toast.makeText(this, "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                            PERMISSION_REQUEST_DEVICE_ID);
                }
            }


//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    != PackageManager.PERMISSION_GRANTED) {
//
//                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    Toast.makeText(this, "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
//                } else {
//                    // No explanation needed; request the permission
//                    ActivityCompat.requestPermissions(this,
//                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                            PERMISSION_REQUEST_FILE_PERMISSION_ID);
//                }
//            }

            //load initial fragment
            //if user logs out, start from the login screen
            String pin = SessionManager.getInstance(this).getPIN();
            String token = SessionManager.getInstance(this).getCustomerToken();
            String mid = SessionManager.getInstance(this).getMerchantID();
            if (Utility.isEmpty(SessionManager.getInstance(this).getPIN())
                    || SessionManager.getInstance(this).getPIN().equals(Constants.SHARED_PREFERENCES_PIN_USER_LOGS_OUT)) {
                LoginFragment login_fragment = new LoginFragment();
                replaceFragment(login_fragment);
                return;
            }

            //if usr already logged in and created a pin, redirect him to enter pin
            if (SessionManager.getInstance(this).isLoginSessionValid() && !Utility.isEmpty(SessionManager.getInstance(this).getPIN())
                    && !SessionManager.getInstance(this).getPIN().equals(Constants.SHARED_PREFERENCES_PIN_USER_LOGS_OUT)) {
                //clearBackStack();
                PinEnterFragment pin_fragment = new PinEnterFragment();
                replaceFragment(pin_fragment);
                return;
            }

            //else load login fragment
            clearBackStack();
            LoginFragment fragment = new LoginFragment();
            replaceFragment(fragment);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(this, "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    PERMISSION_REQUEST_DEVICE_ID);
        }

//        if (requestCode == PERMISSION_REQUEST_FILE_PERMISSION_ID &&
//                (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
//            Toast.makeText(this, "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    PERMISSION_REQUEST_FILE_PERMISSION_ID);
//        }

    }

    @Override
    public void onBackPressed() {

        try {
            getMerchantData();

            if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                mDrawerLayout.closeDrawer(GravityCompat.START);
            } else {

                FragmentManager fm = getSupportFragmentManager();

                if (fm == null || fm.getBackStackEntryCount() <= 1) {
                    finish();
                    return;
                }

                FragmentManager.BackStackEntry entry1 = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
                //if user go background and come foreground, ask for pin every time and don't let them skip it
                if (fm != null && entry1 != null && !Utility.isEmpty(entry1.getName()) && fm.getBackStackEntryCount() > 1
                        && fm.findFragmentByTag(entry1.getName()) instanceof PinEnterFragment) {
                    clearBackStack();
                    finish();
                    return;
                }

                if (fm != null && fm.getBackStackEntryCount() > 1) {
                    FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2);
                    Fragment fragment = null;
                    fragment = fm.findFragmentByTag(entry.getName());

                    if (fragment != null && (fragment instanceof LoginFragment || fragment instanceof HelpFragment || fragment instanceof CreatePinFragment
                            || fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment
                            || fragment instanceof RegisterBankAccFragment)) {
                        hideOrShowVerifiedFunctions(false);

                        if (fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment)
                            mLogoutContainerFL.setVisibility(View.VISIBLE);


                    } else hideOrShowVerifiedFunctions(true);
                }

                super.onBackPressed();
            }

        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.nav_header_img_close:
                if (mDrawerLayout != null) mDrawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.act_home_txt_change_pin:
                ChangePinFragment change_pin = new ChangePinFragment();
                //clearBackStack();
                replaceFragment(change_pin);
                break;

            case R.id.act_home_txt_change_bank:
                ChangeBankAccConfirmFragment change_bank = new ChangeBankAccConfirmFragment();
                replaceFragment(change_bank);
                break;

            case R.id.act_home_txt_help:
                HelpFragment help = new HelpFragment();
                replaceFragment(help);
                break;

            case R.id.nav_head_back_to_login:
                LoginFragment login = new LoginFragment();
                clearBackStack();
                replaceFragment(login);
                break;

            case R.id.act_home_txt_logout:

                try {
                    SessionManager.getInstance(this).savePIN(Constants.SHARED_PREFERENCES_PIN_USER_LOGS_OUT);//clear user pin before logout
                } catch (Exception e) {
                    e.printStackTrace();
                }
                LoginFragment login1 = new LoginFragment();
                clearBackStack();
                replaceFragment(login1);

                break;
        }
    }


    @Override
    public void onRetrofitSuccess(Call<Merchant> call, Merchant response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        mCurrentMerchantDetails = null;

        switch (request_id) {

            case RETROFIT_REQUEST_USER_LOGIN:

                try {

                    if (response != null && !Utility.isEmpty(response.getEmail()) && !Utility.isEmpty(response.getNic())) {
                        mCurrentMerchantDetails = response;

                        String merchant_id = SessionManager.getInstance(this).getMerchantID();
                        mMerchantNameTxt.setText(response.getFull_name());
                        mMerchantIDTxt.setText(Utility.isEmpty(merchant_id) ? "" : merchant_id);
                    } else {
                        Toast.makeText(this, "Merchant details not available. Please try again.", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Merchant details not available. Please try again.", Toast.LENGTH_LONG).show();
                }

                break;

        }

        showMerchantDetails();//show or hide merchant details

    }

    @Override
    public void onRetrofitFailed(Call<Merchant> call) {
        mCurrentMerchantDetails = null;
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(this, "Merchant details not available. Please try again.", Toast.LENGTH_LONG).show();

        showMerchantDetails();//show or hide merchant details
    }


    /**
     * add new fragment to the container and remove existing one, if any
     */
    public void replaceFragment(Fragment fragment) {

        try {
            showMerchantDetails();//show or hide merchant details
            hideOrShowVerifiedFunctions(true);

            showMerchantDetails();//show or hide merchant details

            //if this fragment already exists, skip
            FragmentManager fm = getSupportFragmentManager();
            if (fm != null && fm.getBackStackEntryCount() > 0
                    && fragment.getClass().getName().equals(fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName())) {
                return;
            }

            //check for invalid login session
            if (!(fragment instanceof LoginFragment || fragment instanceof HelpFragment || fragment instanceof CreatePinFragment
                    || fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment)) {

                SessionManager session = SessionManager.getInstance(this);
                if (!session.isLoginSessionValid()) {
                    Toast.makeText(this, "Login session invalid.", Toast.LENGTH_LONG).show();
                    clearBackStack();
                    PinEnterFragment pin = new PinEnterFragment();
                    replaceFragment(pin);

                    return;
                } else {
                    getMerchantData();
                }

            } else {
                hideOrShowVerifiedFunctions(false);

                if (fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment)
                    mLogoutContainerFL.setVisibility(View.VISIBLE);
            }

            //check for invalid bank registration
            if (!(fragment instanceof LoginFragment || fragment instanceof HelpFragment || fragment instanceof CreatePinFragment
                    || fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment
                    || fragment instanceof RegisterBankAccFragment)) {

                SessionManager session = SessionManager.getInstance(this);
                if (!session.isLoginSessionValid()) {
                    Toast.makeText(this, "Bank account not registered. Please register at least one bank account to continue.", Toast.LENGTH_LONG).show();
                    clearBackStack();
                    RegisterBankAccFragment register_bank = new RegisterBankAccFragment();
                    replaceFragment(register_bank);
                    return;
                }

            } else {
                hideOrShowVerifiedFunctions(false);

                if (fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment)
                    mLogoutContainerFL.setVisibility(View.VISIBLE);
            }

            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(mFragContainer.getId(), fragment, fragment.getClass().getName());
            transaction.addToBackStack(fragment.getClass().getName());
            transaction.commit();

            if (mDrawerLayout != null)
                mDrawerLayout.closeDrawer(GravityCompat.START);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * clear back stack
     */
    public void clearBackStack() {

        //clear back-stack
        FragmentManager fm = getSupportFragmentManager();

        if (fm != null && fm.getBackStackEntryCount() > 0)
            for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                fm.popBackStack();
            }
    }

    public void clearBackStackWithSteps(int step) {

        //clear back-stack
        FragmentManager fm = getSupportFragmentManager();

        if (fm != null && fm.getBackStackEntryCount() > 0 && fm.getBackStackEntryCount() < step)
            for (int i = 0; i < step; ++i) {
                fm.popBackStackImmediate();
            }
    }

    /**
     * go one step back
     */
    public void goBack() {
        getMerchantData();

        //clear back-stack
        FragmentManager fm = getSupportFragmentManager();

        if (fm != null && fm.getBackStackEntryCount() > 0)
            fm.popBackStackImmediate();

        if (fm == null || (fm.getBackStackEntryCount() < 1)) {
            finish();
        } else {

            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1);
            Fragment fragment = null;
            if (fm != null) fragment = fm.findFragmentByTag(entry.getName());

            if (fragment != null && (fragment instanceof LoginFragment || fragment instanceof HelpFragment || fragment instanceof CreatePinFragment
                    || fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment
                    || fragment instanceof RegisterBankAccFragment)) {
                hideOrShowVerifiedFunctions(false);

                if (fragment instanceof PinEnterFragment || fragment instanceof ChangePinFragment)
                    mLogoutContainerFL.setVisibility(View.VISIBLE);

            } else hideOrShowVerifiedFunctions(true);

        }
    }

    /**
     * get current back stack count
     **/
    public int getCurrentBackStackCount() {
        //clear back-stack
        FragmentManager fm = getSupportFragmentManager();

        if (fm != null) return fm.getBackStackEntryCount();

        return 0;
    }

    /**
     * set application title
     */
    public void setTitle(String title) {

        if (mToolBar == null) return;

        mToolBar.setTitle(title);

    }

    /**
     * change application title background color
     **/
    public void setTitleBackgroundColor(int color) {
        if (mToolBar == null) return;

        mToolBar.setBackgroundColor(color);
    }

    /**
     * get current logged in merchant's details
     **/
    public Merchant getCurrentMerchantData() {
        return mCurrentMerchantDetails;
    }

    /**
     * hide or show verified functions
     **/
    private void hideOrShowVerifiedFunctions(boolean shouldShow) {
        mChangePinContainerFL.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
        mChangeBankContainerFL.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
        mLogoutContainerFL.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
    }

    /**
     * show or hide merchant details view
     **/
    private void showMerchantDetails() {
        mMerchantDataContainer.setVisibility(View.VISIBLE);
        mSpace1.setVisibility(View.VISIBLE);
        mSpace2.setVisibility(View.VISIBLE);
        if (mCurrentMerchantDetails == null) {
            mMerchantDataContainer.setVisibility(View.GONE);
            //mSpace1.setVisibility(View.GONE);
            //mSpace2.setVisibility(View.GONE);
        }
    }

    /**
     * get merchant details
     **/
    private void getMerchantData() {

        try {

            if (mCurrentMerchantDetails != null) return;

            mCurrentMerchantDetails = null;

            SessionManager session = SessionManager.getInstance(this);
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)) {
                clearBackStack();
                PinEnterFragment pin = new PinEnterFragment();
                replaceFragment(pin);
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            mProgressDialog.show();

            Map<String, Object> merchant = new HashMap<>();
            merchant.put("merchant_id", merchant_id);
            merchant.put("token", customer_token);
            merchant.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", customer_token);

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, this).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getCurrentMerchantDetails(header, merchant_id, merchant), this,
                    RETROFIT_REQUEST_USER_LOGIN);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Merchant details not available. Please try again.", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }

}
