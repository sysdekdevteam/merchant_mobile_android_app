package com.redapplemart.rukula.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.pojo.Bank;

import java.util.ArrayList;

public class BankDetailsAdapter extends ArrayAdapter<Bank> {

    public final static int BANK_DETAILS_ADAPTER_TYPE_BANK = 0;
    public final static int BANK_DETAILS_ADAPTER_TYPE_BRANCH = 1;

    private ArrayList<Bank> mBankDetails;
    private LayoutInflater mInflater;
    private int mAdapterType;

    public BankDetailsAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Bank> objects
            , int type) {
        super(context, resource, objects);

        mBankDetails = objects;
        mInflater = LayoutInflater.from(context);
        mAdapterType = type;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_spinner_item, parent, false);
            holder = new ViewHolder();

            holder.text = convertView.findViewById(R.id.layout_spinner_item_txtview);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        try {

            if (mBankDetails == null || mBankDetails.size() == 0) return convertView;

            Bank bank = mBankDetails.get(position);
            if (mAdapterType == BANK_DETAILS_ADAPTER_TYPE_BANK)
                holder.text.setText(bank.getBank());
            if (mAdapterType == BANK_DETAILS_ADAPTER_TYPE_BRANCH)
                holder.text.setText(bank.getBranch_name());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_spinner_item, parent, false);
            holder = new ViewHolder();

            holder.text = convertView.findViewById(R.id.layout_spinner_item_txtview);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        if (mBankDetails == null || mBankDetails.size() == 0) return convertView;

        Bank bank = mBankDetails.get(position);
        if (mAdapterType == BANK_DETAILS_ADAPTER_TYPE_BANK)
            holder.text.setText(bank.getBank());
        if (mAdapterType == BANK_DETAILS_ADAPTER_TYPE_BRANCH)
            holder.text.setText(bank.getBranch_name());

        return convertView;
    }

    private static class ViewHolder {
        TextView text;
    }

}
