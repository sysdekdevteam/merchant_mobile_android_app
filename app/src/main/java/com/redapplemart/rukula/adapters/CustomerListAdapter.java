package com.redapplemart.rukula.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.pojo.Customer;

import java.util.ArrayList;

public class CustomerListAdapter extends ArrayAdapter<Customer> {
    public static final int CUSTOMER_LIST_SELECT_TYPE_MORE_INFO = 0;
    public static final int CUSTOMER_LIST_SELECT_TYPE_ACC_PAY = 1;

    private Context mContext;
    private ArrayList<Customer> mAllCustomerList;
    private LayoutInflater mLayoutInflater;
    private CustomerListAdapter.CustomerListAdapterItemSelectListener mListener;

    public CustomerListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Customer> objects,
                               CustomerListAdapter.CustomerListAdapterItemSelectListener listener) {
        super(context, resource, objects);

        mContext = context;
        mAllCustomerList = objects;
        mLayoutInflater = LayoutInflater.from(context);
        mListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.customer_list_item, parent, false);
            holder = new ViewHolder();

            holder.customerFName = convertView.findViewById(R.id.lay_customer_list_item_txtview_f_name);
            holder.customerLName = convertView.findViewById(R.id.lay_customer_list_item_txtview_l_name);
            holder.product = convertView.findViewById(R.id.lay_customer_list_item_txtview_products);
            holder.date = convertView.findViewById(R.id.lay_customer_list_item_txtview_application_date);

            holder.moreInfoContainer = convertView.findViewById(R.id.lay_customer_list_item_fr_lay_more_info_container);
            holder.moreInfo = convertView.findViewById(R.id.lay_customer_list_item_txtview_more_info);
            holder.moreInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null)
                        mListener.onItemSelect(position, CUSTOMER_LIST_SELECT_TYPE_MORE_INFO);
                }
            });

            holder.acceptPayContainer = convertView.findViewById(R.id.lay_customer_list_item_fr_lay_acc_payment_container);
            holder.acceptPay = convertView.findViewById(R.id.lay_customer_list_item_txtview_acc_payment);
            holder.acceptPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null)
                        mListener.onItemSelect(position, CUSTOMER_LIST_SELECT_TYPE_ACC_PAY);
                }
            });

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {

            if (mAllCustomerList == null || mAllCustomerList.size() == 0 || mAllCustomerList.size() < position)
                return convertView;

            Customer customer = mAllCustomerList.get(position);

            holder.customerFName.setText(customer.getFirst_name());
            holder.customerLName.setText(customer.getLast_name());
            holder.product.setText(customer.getProduct_name());
            holder.date.setText(String.format("Application Date : %s", customer.getLast_date_of_purchase()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView customerFName;
        TextView customerLName;
        TextView product;
        TextView date;

        FrameLayout moreInfoContainer;
        FrameLayout acceptPayContainer;

        TextView moreInfo;
        TextView acceptPay;
    }

    public interface CustomerListAdapterItemSelectListener {
        void onItemSelect(int position, int type);
    }
}
