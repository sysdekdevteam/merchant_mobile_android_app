package com.redapplemart.rukula.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.util.Utility;

import java.util.ArrayList;

public class MyApplicationsListAdapter extends ArrayAdapter<Customer> {

    private Context mContext;
    private ArrayList<Customer> mAllCustomerList;
    private LayoutInflater mLayoutInflater;

    public MyApplicationsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Customer> objects) {
        super(context, resource, objects);

        mContext = context;
        mAllCustomerList = objects;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.layout_my_applications_list_item, parent, false);
            holder = new ViewHolder();

            holder.name = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_name);
            holder.nic = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_nic);
            holder.app_id = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_application_id);
            holder.status = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_status);
            holder.date = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_purchase_date);
            holder.prod = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_product);
            holder.total = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_total);
            holder.deposit = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_deposit);
            holder.installment = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_installment);
            holder.due = convertView.findViewById(R.id.lay_my_applications_list_item_txtview_due_payment);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {

            if (mAllCustomerList == null || mAllCustomerList.size() == 0 || mAllCustomerList.size() < position)
                return convertView;

            Customer customer = mAllCustomerList.get(position);

            holder.name.setText(String.format("%s %s", customer.getFirst_name(), customer.getLast_name()));
            holder.nic.setText(Utility.isEmpty(customer.getNic()) ? "*********" : customer.getNic());
            holder.app_id.setText(customer.getApplication_id());

            holder.status.setText(Utility.isEmpty(customer.getStatus()) ? "Incomplete" : customer.getStatus());
            if (!Utility.isEmpty(customer.getStatus()) && customer.getStatus().equals("Complete"))
                holder.status.setTextColor(mContext.getResources().getColor(R.color.green_color_2));
            else
                holder.status.setTextColor(mContext.getResources().getColor(R.color.red_color_1));

            holder.date.setText(customer.getLast_date_of_purchase());
            holder.prod.setText(customer.getProduct_name());
            holder.total.setText(customer.getTotal_outstanding());
//            holder.deposit.setText(customer.getOutstanding_as_of_today());
            holder.installment.setText(customer.getInstallment_value());
            holder.due.setText(customer.getTotal_outstanding());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView nic;
        TextView app_id;
        TextView status;
        TextView date;
        TextView prod;
        TextView total;
        TextView deposit;
        TextView installment;
        TextView due;
    }
}
