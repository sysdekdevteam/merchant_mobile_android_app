package com.redapplemart.rukula.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.pojo.Transaction;

import java.util.ArrayList;

public class MyTransactionsListAdapter extends ArrayAdapter<Transaction> {

    private Context mContext;
    private ArrayList<Transaction> mAllTransactionsList;
    private LayoutInflater mLayoutInflater;

    public MyTransactionsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<Transaction> objects) {
        super(context, resource, objects);

        mContext = context;
        mAllTransactionsList = objects;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.layout_my_transactions_list_item, parent, false);
            holder = new ViewHolder();

            holder.customerName = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_name);
            holder.product = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_product);
            holder.nic = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_nic);
            holder.date = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_date);
            holder.transaction_id = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_tran_id);
            holder.payment = convertView.findViewById(R.id.lay_my_trans_list_item_txtview_payment);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {

            if (mAllTransactionsList == null || mAllTransactionsList.size() == 0 || mAllTransactionsList.size() < position)
                return convertView;

            Transaction transaction = mAllTransactionsList.get(position);

            holder.customerName.setText(transaction.getCustomer_name());
            holder.date.setText(transaction.getDate_and_time());
            holder.transaction_id.setText(String.valueOf(transaction.getTransaction_id()));
            holder.payment.setText(transaction.getAmount());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView customerName;
        TextView product;
        TextView nic;
        TextView date;
        TextView transaction_id;
        TextView payment;
    }
}
