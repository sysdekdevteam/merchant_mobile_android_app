package com.redapplemart.rukula.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.pojo.PendingTransactions;

import java.util.ArrayList;

public class TransactionsListAdapter extends ArrayAdapter<PendingTransactions> {

    private ArrayList<PendingTransactions> mPendingTransactionsList;
    private LayoutInflater mLayoutInflater;
    private TransactionListAdapterItemSelectListener mListener;

    public TransactionsListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<PendingTransactions> objects,
                                   TransactionListAdapterItemSelectListener listener) {
        super(context, resource, objects);

        mPendingTransactionsList = objects;
        mLayoutInflater = LayoutInflater.from(context);
        mListener = listener;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = mLayoutInflater.inflate(R.layout.accept_payment_list_item, parent, false);
            holder = new ViewHolder();

            holder.customerName = convertView.findViewById(R.id.lay_accept_palyment_list_item_txtview_cus_name);
            holder.dateTime = convertView.findViewById(R.id.lay_accept_palyment_list_item_txtview_date_time);
            holder.NIC = convertView.findViewById(R.id.lay_accept_palyment_list_item_txtview_nic);
            holder.amount = convertView.findViewById(R.id.lay_accept_palyment_list_item_txtview_amount);

            holder.moreInfo = convertView.findViewById(R.id.lay_accept_palyment_list_item_txtview_more_info);
            holder.moreInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) mListener.onItemSelect(position);
                }
            });

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {

            if (mPendingTransactionsList == null || mPendingTransactionsList.size() == 0 || mPendingTransactionsList.size() < position)
                return convertView;

            PendingTransactions transaction = mPendingTransactionsList.get(position);

            holder.customerName.setText(transaction.getName());
            holder.dateTime.setText(transaction.getDate_and_time());
            holder.NIC.setText(transaction.getNic());
            holder.amount.setText(String.format("RS. %s", transaction.getAmount()));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private static class ViewHolder {
        TextView customerName;
        TextView dateTime;
        TextView NIC;
        TextView amount;

        TextView moreInfo;
    }

    public interface TransactionListAdapterItemSelectListener {
        void onItemSelect(int position);
    }
}
