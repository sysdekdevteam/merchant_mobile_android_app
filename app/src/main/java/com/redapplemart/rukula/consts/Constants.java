package com.redapplemart.rukula.consts;

/**
 * Created by SYSDEK Pvt Ltd on 6/20/17.
 */

public enum Constants {
    SAMPLE;

    /**
     * HTTP connection related constants
     **/
    public final static String BASE_URL = "https://sampathapi.rukulalkapps.com/";
    public final static String RUKULA_BASE_URL = "https://rukulapay.rukulalkapps.com/";
//    public final static String BASE_URL = "https://uatweb.sampath.lk/";
//    public final static String RUKULA_BASE_URL = "http://staging.rukulalkapps.com/";

    /**
     * API authentication related constants
     **/
    public final static String API_AUTH_USER_NAME = "GoGoCargo";
    public final static String API_AUTH_USER_PASSWORD = "JBz3HJsTusqetcbr";

    /**
     * customer/account registration codes
     **/
    public final static String CUSTOMER_REG_CODE_INITIAL_REG = "01";
    public final static String CUSTOMER_REG_CODE_ACC_REG = "02";

    /**
     * API response codes
     **/
    public static final String API_RESPONSE_STRING_SUCCESS = "success";
    public static final String API_RESPONSE_STRING_FAIL = "failed";
    //pin responses
    public static final int API_RESPONSE_PIN_BLOCKED = -1;//max attempts reached
    public static final int API_RESPONSE_PIN_INCORRECT = 0;
    public static final int API_RESPONSE_PIN_SUCCESS = 1;
    public static final int API_RESPONSE_PIN_TEMP = 2;
    public static final int API_RESPONSE_SUCCESS_AND_PIN_CONFIGURED = 3;


    /**
     * search customer details response codes
     **/
    public static final int CUSTOMER_SEARCH_RESP_CODE_NOT_REGISTERED = 0;
    public static final int CUSTOMER_SEARCH_RESP_CODE_REGISTERED = 1;

    /**
     * just pay and sampath verification codes
     **/
    public final static String SAMPATH_VERIFICATION_MERCHANT_ID = "0000000066";
    public final static String SAMPATH_VERIFICATION_MERCHANT_TOKEN = "UtLJQ5Dq6NqHaFZzAnlHFw==";
    public final static String JUST_PAY_VERIFICATION_CODE = "6278_M90965_70444";


    /**
     * custom application UIDs
     **/
    //shared preferences UID
    public final static String CUSTOM_APPLICATION_UID_SHARED_PREFERENCES = "lk.rukula.rukulapay.sharedpref";

    /**
     * shared preferences saved items UIDs
     **/
    //sign-up and sign-in response details
    public final static String SHARED_PREFERENCES_REF_ID = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "refid");
    public final static String SHARED_PREFERENCES_USER_CHALLENGE = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "userchallenge");
    public final static String SHARED_PREFERENCES_ACCOUNT_NO = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "accountref");
    public final static String SHARED_PREFERENCES_JUST_PAY_CUSTOMER_TOKEN = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "justpaycustomertoken");
    public final static String SHARED_PREFERENCES_JUST_PAY_ACCOUNT_REF = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "justpayaccountref");
    public final static String SHARED_PREFERENCES_CUSTOMER_TOKEN = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "customertoken");
    public final static String SHARED_PREFERENCES_MERCHANT_ID = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "merchantid");
    public final static String SHARED_PREFERENCES_PIN = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "pin");
    public final static String SHARED_PREFERENCES_CUSTOMER_NIC = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "customernic");
    public final static String SHARED_PREFERENCES_CUSTOMER_CONSENT = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "randomamount");
    public final static String SHARED_PREFERENCES_CUSTOMER_CUSTOMERS = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "customeraccounts");

    public final static String SHARED_PREFERENCES_PIN_USER_LOGS_OUT = String.format("%s.%s", CUSTOM_APPLICATION_UID_SHARED_PREFERENCES, "pinuserlogout");

    /**
     * current transaction constants
     **/
    public final static String CURRENT_CUSTOMER_REG_UID_PREFIX = "JP_CX_";
    public final static String CURRENT_TRANSACTION_UID_PREFIX = "JP_TX_";

    /**
     * random amount limitation
     **/
    public final static int RANDOM_AMOUNT_MIN_VALUE = 2;
    public final static int RANDOM_AMOUNT_MAX_VALUE = 99;

}
