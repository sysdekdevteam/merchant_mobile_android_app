package com.redapplemart.rukula.consts;

public enum CustomerLookupSort {
    SORT_NONE,
    A_TO_Z_NAME_ORDER,
    Z_TO_A_NAME_ORDER,
    INSTALLMENT_AMOUNTS,
    LATEST_APPLICATION
}
