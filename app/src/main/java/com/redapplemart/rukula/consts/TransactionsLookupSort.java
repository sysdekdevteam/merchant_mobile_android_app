package com.redapplemart.rukula.consts;

public enum TransactionsLookupSort {
    SORT_NONE,
    CUSTOMER_NAME_DESC,
    CUSTOMER_NAME_ASC,
    NIC_DESC,
    NIC_ASC,
    PAY_DATE_DESC,
    PAY_DATE_ASC
}
