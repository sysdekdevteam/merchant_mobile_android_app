package com.redapplemart.rukula.controllers;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.redapplemart.rukula.consts.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by SYSDEK Pvt Ltd on 7/27/17.
 * <p>
 * this is a singleton, that contains all Retrofit Http request functions
 */

public class RetrofitController {

    private static RetrofitController mRetrofitController;
    //retrofit http connection object
    private Retrofit mRetrofit;
    //custom host change interceptor object
    private static HostSelectionInterceptor mHostSelectionInterceptor;
    //base url
    private static String mBaseURL;

    private static Context mContext;

    //create new instance if needed
    static {

    }

    /**
     * return the singleton object representation of this class
     **/
    public static RetrofitController getInstance(String base_url, Context context) throws Exception {
        mContext = context;

        try {
            if (mBaseURL == null || !mBaseURL.equals(base_url))
                mRetrofitController = new RetrofitController(base_url);
            mBaseURL = base_url;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mRetrofitController;
    }

    /**
     * change current API's base url
     *
     * @param base_url current request's base url
     **/
    public RetrofitController changeBaseURL(String base_url) {

        //set new host name
        if (mHostSelectionInterceptor != null)
            mHostSelectionInterceptor.mCustomURL = base_url;

        return mRetrofitController;
    }

    /**
     * create new retrofit object with custom interceptor for changing host
     **/
    private RetrofitController(String base_url) throws Exception {

        HttpLoggingInterceptor.Logger fileLogger = new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String s) {
                Log.e("nmj retrofit", s);
//                writeToSDFile(s);
            }
        };

        //enable retrofit logging ability
        HttpLoggingInterceptor log_interceptor = new HttpLoggingInterceptor(fileLogger);
        log_interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(new Interceptor() {
//                    @Override
//                    public Response intercept(Chain chain) throws IOException {
//
//                        String basic_auth = "basic " +
//                                Base64.encodeToString(String.format("%s:%s", Constants.API_AUTH_USER_NAME, Constants.API_AUTH_USER_PASSWORD).getBytes()
//                                        , Base64.NO_WRAP);
//
//                        Request request = chain.request().newBuilder().addHeader("Authorization", basic_auth).build();
//                        return chain.proceed(request);
//                    }
//                })
                .writeTimeout(5 * 60, TimeUnit.SECONDS)
                .readTimeout(5 * 60, TimeUnit.SECONDS)
                .connectTimeout(5 * 60, TimeUnit.SECONDS)
                .addInterceptor(log_interceptor)
                .build();


        //create new retrofit engine
        mRetrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

    }

    /**
     * get current retrofit engine object
     **/
    public Retrofit getRetrofitEngine() {
        return mRetrofit;
    }


    /**
     * this interceptor is use to change host name according to the apis
     **/
    private class HostSelectionInterceptor implements Interceptor {

        //base url for the APIs
        private volatile String mCustomURL;

        @Override
        public Response intercept(Chain chain) throws IOException {

            Request request = chain.request();
            if (this.mCustomURL != null && !this.mCustomURL.isEmpty()) {

                String basic_auth = "basic " +
                        Base64.encodeToString(String.format("%s:%s", Constants.API_AUTH_USER_NAME, Constants.API_AUTH_USER_PASSWORD).getBytes()
                                , Base64.NO_WRAP);

                request = request.newBuilder()
//                        .url(this.mCustomURL)
                        .addHeader("Authorization", basic_auth)
                        .build();
            }
            return chain.proceed(request);

        }
    }

    /**
     * Method to write ascii text characters to file on SD card. Note that you must add a
     * WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     * a FileNotFound Exception because you won't have write permission.
     */
    private void writeToSDFile(String s) {

        try {
            // Find the root of the external storage.
            // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

            File root = android.os.Environment.getExternalStorageDirectory();

            // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

            File dir = new File(root.getAbsolutePath() + "/download");
            dir.mkdirs();
            File file = new File(dir, "myData.txt");

            FileOutputStream f = new FileOutputStream(file, true);
            PrintWriter pw = new PrintWriter(f);
            pw.append(s);
            pw.append("\n");
            pw.flush();
            pw.close();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
