package com.redapplemart.rukula.controllers;

import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SYSDEK Pvt Ltd on 7/31/17.
 */

public class RetrofitResponseController {

    /**
     * execute selected http API call
     *
     * @param call       current http request call
     * @param listener   current user created listener
     * @param request_id user provided custom request id to identify the executed request
     **/
    public static <T> boolean executeCall(Call<T> call, final RetrofitResponseInterface listener,
                                          final int request_id) {

        if (call == null || listener == null) return false;

        try {

            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, Response<T> response) {
                    //process response
                    createValidResponse(call, response, listener, request_id);
                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {
                    //process response
                    createValidResponse(call, null, listener, request_id);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    /**
     * create valid response from retrieved http response
     *
     * @param call       current http request call
     * @param response   response retrieved from http call
     * @param listener   current user created listener
     * @param request_id user provided custom request id to identify the executed request
     **/
    private static <T> void createValidResponse(Call<T> call, Response<T> response
            , RetrofitResponseInterface listener, int request_id) {

        try {

            if (listener != null) listener.onRetrofitSuccess(call, response.body(), request_id);

        } catch (Exception e) {
            e.printStackTrace();

            //call failed response
            if (listener != null) listener.onRetrofitFailed(call);
        }

    }

}
