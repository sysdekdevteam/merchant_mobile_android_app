package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.PendingTransactions;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcceptPaymentFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<PendingTransactions> {
    private static final int RETROFIT_REQUEST_PENDING_TRANSACTIONS = 0;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    public AcceptPaymentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_accept_payment, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        LinearLayout pending = base_view.findViewById(R.id.frag_accept_pay_lin_lay_pending);
        LinearLayout request = base_view.findViewById(R.id.frag_accept_pay_lin_lay_request);
        TextView back = base_view.findViewById(R.id.frag_accept_pay_txt_back);
        pending.setOnClickListener(this);
        request.setOnClickListener(this);
        back.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_accept_pay_img_pending);
        ImageView img2 = base_view.findViewById(R.id.frag_accept_pay_img_cash);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_pay);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_cash);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Payment Options");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_accept_pay_lin_lay_pending:
                checkForPendingTransactions();//check and request for pending transactions
                break;

            case R.id.frag_accept_pay_lin_lay_request:
                if (homeActivity != null) {
                    CustomerSearchNICFragment fragment = new CustomerSearchNICFragment();
                    fragment.isCustomerSearch = false;
                    homeActivity.replaceFragment(fragment);
                }
                break;

            case R.id.frag_accept_pay_txt_back:
                if (homeActivity != null) homeActivity.goBack();
                break;
        }
    }


    @Override
    public void onRetrofitSuccess(Call<PendingTransactions> call, PendingTransactions response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_PENDING_TRANSACTIONS:

                try {
                    HomeActivity homeActivity = null;
                    if (getActivity() instanceof HomeActivity)
                        homeActivity = (HomeActivity) getActivity();

                    if (response != null && response.getItems() != null) {

                        //if multiple items available
                        if (response.getItems().size() > 1) {

                            if (homeActivity != null) {
                                AcceptPaymentListFragment fragment = new AcceptPaymentListFragment();
                                homeActivity.replaceFragment(fragment);
                            }

                        } else if (response.getItems().size() == 1) {//if single item available

                            if (homeActivity != null) {
                                PendingTransactionFragment fragment = new PendingTransactionFragment();
                                fragment.mSelectedTransactionID = response.getItems().get(0).getId();
                                homeActivity.replaceFragment(fragment);
                            }

                        }

                        return;

                    }

                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<PendingTransactions> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();

    }


    /**
     * check for pending transactions to decide the showing UI
     **/
    private void checkForPendingTransactions() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", mSessionManager.getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("page", 1);
            transaction_req.put("per_page", 100);

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getPendingTransactions(header, transaction_req), this,
                    RETROFIT_REQUEST_PENDING_TRANSACTIONS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }
}
