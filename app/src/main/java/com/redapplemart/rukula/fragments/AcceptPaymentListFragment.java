package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.adapters.TransactionsListAdapter;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.PendingTransactions;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class AcceptPaymentListFragment extends Fragment implements View.OnClickListener
        , RetrofitResponseInterface<PendingTransactions>, TransactionsListAdapter.TransactionListAdapterItemSelectListener {
    private static final int RETROFIT_REQUEST_PENDING_TRANSACTIONS = 0;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private ListView mTransactionsListView;
    private TransactionsListAdapter mTransactionsListAdapter;

    public AcceptPaymentListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_accept_payment_list, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mTransactionsListView = base_view.findViewById(R.id.frag_accept_payment_list_list_view);
        TextView back_btn = base_view.findViewById(R.id.frag_accept_payment_list_txtview_back);
        back_btn.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Pending Transactions");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            //request for all available items list
            requestPendingTransactions();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.frag_accept_payment_list_txtview_back:
                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                if (homeActivity != null) homeActivity.goBack();
                break;
        }
    }


    @Override
    public void onItemSelect(int position) {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null && mTransactionsListAdapter.getCount() >= position && mTransactionsListAdapter.getItem(position) != null) {
                PendingTransactionFragment fragment = new PendingTransactionFragment();
                fragment.mSelectedTransactionID = mTransactionsListAdapter.getItem(position).getId();
                homeActivity.replaceFragment(fragment);
                return;
            }

            Toast.makeText(getActivity(), "can't open the transaction details page. please try again.", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(getActivity(), "can't open the transaction details page. please try again.", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onRetrofitSuccess(Call<PendingTransactions> call, PendingTransactions response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_PENDING_TRANSACTIONS:

                try {

                    if (response != null && response.getItems() != null) {

                        //if multiple items available
                        if (response.getItems().size() > 0 && getActivity() != null) {

                            //create list adapter and add to list view
                            mTransactionsListAdapter = new TransactionsListAdapter(getActivity(), R.layout.accept_payment_list_item
                                    , response.getItems(), this);
                            mTransactionsListView.setAdapter(mTransactionsListAdapter);
                            mTransactionsListAdapter.notifyDataSetChanged();

                            return;
                        }

                    }

                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<PendingTransactions> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();

    }


    /**
     * check for pending transactions to decide the showing UI
     **/
    private void requestPendingTransactions() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", mSessionManager.getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("page", 1);
            transaction_req.put("per_page", 100);

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getPendingTransactions(header, transaction_req), this,
                    RETROFIT_REQUEST_PENDING_TRANSACTIONS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }
}
