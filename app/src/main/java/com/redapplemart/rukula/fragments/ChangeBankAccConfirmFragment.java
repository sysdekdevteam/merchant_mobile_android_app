package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangeBankAccConfirmFragment extends Fragment implements View.OnClickListener {


    public ChangeBankAccConfirmFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_change_bank_acc_confirm, container, false);

        TextView yes = base_view.findViewById(R.id.frag_change_bank_acc_confirm_yes_txt_btn);
        TextView no = base_view.findViewById(R.id.frag_change_bank_acc_confirm_no_txt_btn);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_change_bank_acc_img_1);
        ImageView img2 = base_view.findViewById(R.id.frag_change_bank_acc_img_2);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_banking);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_warn);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.setTitle("Update Bank Account");
            homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_change_bank_acc_confirm_no_txt_btn:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;

            case R.id.frag_change_bank_acc_confirm_yes_txt_btn:
                if (homeActivity != null) {
                    RegisterBankAccFragment fragment = new RegisterBankAccFragment();
                    homeActivity.clearBackStackWithSteps(1);
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }
}
