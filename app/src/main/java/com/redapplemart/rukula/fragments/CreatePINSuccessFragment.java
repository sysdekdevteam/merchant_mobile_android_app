package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lankaclear.justpay.LCTrustedSDK;
import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.UserBankAccount;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePINSuccessFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<UserBankAccount> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_REGISTERED_BANK_ACC = 0;

    public boolean isPinRecreate = false;

    private ProgressDialog mProgressDialog;

    //just pay mobile SDK instance
    private LCTrustedSDK mLCTrustedSDK;

    public CreatePINSuccessFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_create_pinsuccess, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        TextView submit = base_view.findViewById(R.id.frag_create_pin_success_txt_submit);
        submit.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_create_pin_success_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_clap);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Create Your PIN");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mLCTrustedSDK = new LCTrustedSDK(getActivity());
            mLCTrustedSDK.init();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_create_pin_success_txt_submit:
                //check for registered bank accounts
                getUserRegisteredBankAccounts();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<UserBankAccount> call, UserBankAccount response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (request_id) {

            case RETROFIT_REQUEST_REGISTERED_BANK_ACC:

                try {

//                    if (isPinRecreate) {
//
//                        //if user paused app in middle of something, continue from there
//                        if (homeActivity != null && homeActivity.getCurrentBackStackCount() > 1) {
//                            homeActivity.goBack();
//                            return;
//                        }
//
//                        if (homeActivity != null) {
//                            HomeFragment fragment = new HomeFragment();
//                            homeActivity.clearBackStack();
//                            homeActivity.replaceFragment(fragment);
//                        }
//                        return;
//                    }

                    // TODO: 8/20/2018 remove local session value check, after fix the API call
                    if (response == null
                            || (Utility.isEmpty(response.getAccount_reference()) && Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayAccountReference()))
                            || (Utility.isEmpty(response.getCustomer_token())) && Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayCustomerToken())) {

                        if (homeActivity != null) {
                            RegisterBankAccFragment fragment = new RegisterBankAccFragment();
                            homeActivity.clearBackStack();
                            homeActivity.replaceFragment(fragment);
                        }

                        return;
                    }

                    //if user paused app in middle of something, continue from there
//                    if (homeActivity != null && homeActivity.getCurrentBackStackCount() > 1) {
//                        homeActivity.goBack();
//                        return;
//                    }

                    if (!Utility.isEmpty(response.getAccount_reference()))
                        SessionManager.getInstance(getActivity()).saveJustPayAccountReference(response.getAccount_reference());
                    if (!Utility.isEmpty(response.getCustomer_token()))
                        SessionManager.getInstance(getActivity()).saveJustPayCustomerToken(response.getCustomer_token());

                    //if user already registered a bank account, let him access without redirecting to register bank fragment
                    if (homeActivity != null) {
                        HomeFragment fragment = new HomeFragment();
                        homeActivity.clearBackStack();
                        homeActivity.replaceFragment(fragment);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<UserBankAccount> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "PIN creation failed. please try again with valid details.", Toast.LENGTH_LONG).show();
    }

    /**
     * save user registered bank account details
     **/
    private void getUserRegisteredBankAccounts() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(token)) {
                Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> user_data = new HashMap<>();
            user_data.put("merchant_id", merchant_id);
            user_data.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getUserBankDetails(header, user_data), this,
                    RETROFIT_REQUEST_REGISTERED_BANK_ACC);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
