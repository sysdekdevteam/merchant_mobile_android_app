package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.CommonResponse;
import com.redapplemart.rukula.pojo.PIN;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 */
public class CreatePinFragment extends Fragment implements View.OnClickListener
        , RetrofitResponseInterface<CommonResponse> {
    private static final int RETROFIT_REQUEST_USER_PIN = 0;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private EditText mPINEdtxt;
    private EditText mRePINEdtxt;

    public CreatePinFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_create_pin, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        TextView submit = base_view.findViewById(R.id.frag_create_pin_txt_submit);
        submit.setOnClickListener(this);

        mPINEdtxt = base_view.findViewById(R.id.create_pin_fr_edtxt_pin);
        mRePINEdtxt = base_view.findViewById(R.id.create_pin_fr_edtxt_re_pin);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.create_pin_fr_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_key);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.setTitle("Create Your PIN");
            homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.frag_create_pin_txt_submit:
                createPIN();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<CommonResponse> call, CommonResponse response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_USER_PIN:

                try {

                    if (response != null && response.getStatus() != null && !response.getStatus().isEmpty()
                            && response.getStatus().equals(Constants.API_RESPONSE_STRING_SUCCESS)) {

                        //save user token
                        mSessionManager.savePIN(mPINEdtxt.getText().toString());

                        HomeActivity homeActivity = null;
                        if (getActivity() instanceof HomeActivity)
                            homeActivity = (HomeActivity) getActivity();
                        if (homeActivity != null) {

                            //show notification, only if user not set the bank account
                            if (Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayAccountReference())
                                    || Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayCustomerToken())) {
                                CreatePINSuccessFragment fragment = new CreatePINSuccessFragment();
                                fragment.isPinRecreate = false;
                                homeActivity.replaceFragment(fragment);
                            } else {
                                HomeFragment fragment = new HomeFragment();
                                homeActivity.clearBackStack();
                                homeActivity.replaceFragment(fragment);
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "PIN creation failed. please try again with correct details.", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<CommonResponse> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "PIN creation failed. please try again with correct details.", Toast.LENGTH_LONG).show();
    }


    //create pin
    private void createPIN() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (mPINEdtxt.getText() == null || mRePINEdtxt.getText() == null || mPINEdtxt.getText().toString().isEmpty()
                    || mRePINEdtxt.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "please enter valid details and try.", Toast.LENGTH_LONG).show();
                return;
            }

            if (!mPINEdtxt.getText().toString().equals(mRePINEdtxt.getText().toString())) {
                Toast.makeText(getActivity(), "PIN you entered are not matched. please correct them and try.", Toast.LENGTH_LONG).show();
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            mProgressDialog.show();

            PIN pin = new PIN();
            pin.setMerchant_id(mSessionManager.getMerchantID());
            pin.setDevice_id(telephonyManager.getDeviceId());
            pin.setPin(mPINEdtxt.getText().toString());

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.registerPIN(header, pin), this,
                    RETROFIT_REQUEST_USER_PIN);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "PIN creation failed. please try again with correct details.", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }
}
