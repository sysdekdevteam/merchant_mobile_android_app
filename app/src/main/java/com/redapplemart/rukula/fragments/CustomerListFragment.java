package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.adapters.CustomerListAdapter;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.consts.CustomerLookupSort;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.popup.CustomerSortPopup;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerListFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Customer>
        , CustomerListAdapter.CustomerListAdapterItemSelectListener, CustomerSortPopup.CustomerSortPopupInterface {
    private static final int RETROFIT_REQUEST_ALL_CUSTOMERS = 0;

    public String mSearchCondition;

    private ProgressDialog mProgressDialog;

    private ListView mCustomerListView;
    private CustomerListAdapter mCustomerListAdapter;

    private ArrayList<Customer> mAllCustomers;

    public CustomerListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_customer_list, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mCustomerListView = base_view.findViewById(R.id.frag_customer_list_list_view);
        TextView back = base_view.findViewById(R.id.frag_customer_list_txtview_back);
        back.setOnClickListener(this);

        TextView sort = base_view.findViewById(R.id.frag_customer_list_txtview_sort);
        sort.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!Utility.isEmpty(mSearchCondition))
            requestAllCustomers(CustomerLookupSort.SORT_NONE);
        else showErrorAndGoBack(true);

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Customer Information");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.frag_customer_list_txtview_back) {
            showErrorAndGoBack(false);
        }

        if (id == R.id.frag_customer_list_txtview_sort) {
            CustomerSortPopup dialog = new CustomerSortPopup(getActivity());
            dialog.setPopupActionListener(this);
            dialog.show();
        }
    }


    @Override
    public void onItemSelect(int position, int type) {
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        if (mAllCustomers == null || mAllCustomers.size() == 0 || mAllCustomers.size() < position) {
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
            return;
        }

        Customer customer = mAllCustomers.get(position);

        if (type == CustomerListAdapter.CUSTOMER_LIST_SELECT_TYPE_ACC_PAY) {

            if (homeActivity != null) {
                RequestCustomerPaymentFragment fragment = new RequestCustomerPaymentFragment();
                fragment.isExistingCustomer = (customer.getMy_customer() == 1);
                fragment.mCurrentSelectedCustomerData = customer;
                homeActivity.replaceFragment(fragment);
            }
        } else {

            if (homeActivity != null) {
                RequestCustomerInfoFragment fragment = new RequestCustomerInfoFragment();
                fragment.isExistingCustomer = (customer.getMy_customer() == 1);
                fragment.mCurrentSelectedCustomerData = customer;
                homeActivity.replaceFragment(fragment);
            }
        }

    }

    @Override
    public void onSortSelected(CustomerLookupSort sorting) {
        if (!Utility.isEmpty(mSearchCondition))
            requestAllCustomers(sorting);
        else showErrorAndGoBack(true);
    }


    @Override
    public void onRetrofitSuccess(Call<Customer> call, Customer response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_ALL_CUSTOMERS:

                try {

                    if (response != null && response.getItems() != null) {

                        //if multiple items available
                        if (response.getItems().size() > 0 && getActivity() != null) {

                            mAllCustomers = response.getItems();

                            //create list adapter and add to list view
                            mCustomerListAdapter = new CustomerListAdapter(getActivity(), R.layout.customer_list_item
                                    , response.getItems(), this);
                            mCustomerListView.setAdapter(mCustomerListAdapter);
                            mCustomerListAdapter.notifyDataSetChanged();

                            return;
                        }

                    }

                    showErrorAndGoBack(true);

                } catch (Exception e) {
                    e.printStackTrace();
                    showErrorAndGoBack(true);
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Customer> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showErrorAndGoBack(true);
    }


    /**
     * show error and go back
     **/
    private void showErrorAndGoBack(boolean shouldShowError) {
        if (shouldShowError)
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null) {
            homeActivity.goBack();
        }
    }

    /**
     * check for all available customers
     *
     * @param sorting_option current search query's search options
     **/
    private void requestAllCustomers(CustomerLookupSort sorting_option) {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)
                    || Utility.isEmpty(mSearchCondition)) {
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", SessionManager.getInstance(getActivity()).getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("per_page", 100);
            transaction_req.put("page", 1);

            switch (sorting_option) {
                case A_TO_Z_NAME_ORDER:
                    transaction_req.put("sort_field", "first_name");
                    transaction_req.put("sort_order", "asc");
                    break;
                case Z_TO_A_NAME_ORDER:
                    transaction_req.put("sort_field", "first_name");
                    transaction_req.put("sort_order", "desc");
                    break;
                case INSTALLMENT_AMOUNTS:
                    transaction_req.put("sort_field", "installment_value");
                    transaction_req.put("sort_order", "asc");
                    break;
                case LATEST_APPLICATION:
                    transaction_req.put("sort_field", "last_date_of_purchase");
                    transaction_req.put("sort_order", "asc");
                    break;
            }

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.searchAmongAvailableCustomers(header, mSearchCondition, transaction_req), this,
                    RETROFIT_REQUEST_ALL_CUSTOMERS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }
}
