package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomerSearchNICFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Customer> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS = 0;

    public boolean isCustomerSearch = false;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private EditText mNICNo;

    public CustomerSearchNICFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_customer_search_nic, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mNICNo = base_view.findViewById(R.id.frag_customer_search_nic_nic_edt_txt);
        TextView search = base_view.findViewById(R.id.frag_customer_search_nic_search_txt_btn);
        TextView back = base_view.findViewById(R.id.frag_customer_search_nic_back_txt_btn);

        search.setOnClickListener(this);
        back.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_customer_search_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_cash);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                if (isCustomerSearch) homeActivity.setTitle("Customer Search");
                else homeActivity.setTitle("Accept Cash");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {

            case R.id.frag_customer_search_nic_search_txt_btn:
                searchForUser();
                break;

            case R.id.frag_customer_search_nic_back_txt_btn:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;

        }
    }

    @Override
    public void onRetrofitSuccess(Call<Customer> call, Customer response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        if (response == null) {
            Toast.makeText(getActivity(), "Customer not found. Please retry.", Toast.LENGTH_LONG).show();
            return;
        }

        switch (request_id) {

            case RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS:

                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                RequestCustomerPaymentFragment fragment = new RequestCustomerPaymentFragment();

                //check for not registered customers
                if (response.get_meta() != null) {

                    if (mNICNo.getText() == null || Utility.isEmpty(mNICNo.getText().toString())) {
                        Toast.makeText(getActivity(), "Please enter a valid search condition and continue.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (homeActivity != null) {
                        CustomerListFragment cus_fragment = new CustomerListFragment();
                        cus_fragment.mSearchCondition = mNICNo.getText().toString();
                        homeActivity.replaceFragment(cus_fragment);
                    }

                } else if (response.getMy_customer() == Constants.CUSTOMER_SEARCH_RESP_CODE_NOT_REGISTERED) {

                    if (homeActivity != null) {
                        fragment.mCurrentSelectedCustomerData = response;
                        fragment.isExistingCustomer = false;
                        homeActivity.replaceFragment(fragment);
                    }

                } else if (response.getMy_customer() == Constants.CUSTOMER_SEARCH_RESP_CODE_REGISTERED) {//show registered customer

                    if (homeActivity != null) {
                        fragment.mCurrentSelectedCustomerData = response;
                        fragment.isExistingCustomer = true;
                        homeActivity.replaceFragment(fragment);
                    }

                } else {

                    Toast.makeText(getActivity(), "No customers available. Please enter a valid search condition and continue.", Toast.LENGTH_LONG).show();

                }

                break;

        }
    }

    @Override
    public void onRetrofitFailed(Call<Customer> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "Customer not found. Please retry.", Toast.LENGTH_LONG).show();
    }


    /**
     * search for a user with given NIC
     **/
    private void searchForUser() {

        try {

            if (mSessionManager == null)
                mSessionManager = SessionManager.getInstance(getActivity());

            if (mNICNo.getText() == null || mNICNo.getText().toString().isEmpty() || Utility.isEmpty(mSessionManager.getMerchantID())
                    || Utility.isEmpty(mSessionManager.getCustomerToken())) {
                Toast.makeText(getActivity(), "customers not available for this search.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", mSessionManager.getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.searchAmongAvailableCustomers(header, mNICNo.getText().toString(), transaction_req), this,
                    RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "customers not available for this search.", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }
}
