package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends Fragment implements View.OnClickListener {


    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View base_view = inflater.inflate(R.layout.fragment_help, container, false);

        TextView back = base_view.findViewById(R.id.frag_help_back_txt_btn);
        back.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_help_img1);
        ImageView img2 = base_view.findViewById(R.id.frag_help_img2);
        ImageView img3 = base_view.findViewById(R.id.frag_help_img3);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_logo_no_border);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_warn);
        Utility.loadImage(getActivity(), img3, R.drawable.ic_viber);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.setTitle("Merchant App");
            homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.theme_color));
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.frag_help_back_txt_btn) {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();
            if (homeActivity != null) {
                homeActivity.goBack();
            }
        }
    }
}
