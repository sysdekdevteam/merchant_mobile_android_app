package com.redapplemart.rukula.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_home, container, false);

        LinearLayout view_app = base_view.findViewById(R.id.frag_home_lin_lay_view_apps);
        LinearLayout accept_pay = base_view.findViewById(R.id.frag_home_lin_lay_accept_pay);
        LinearLayout new_sale = base_view.findViewById(R.id.frag_home_lin_lay_new_sale);
        LinearLayout my_info = base_view.findViewById(R.id.frag_home_lin_lay_my_info);

        view_app.setOnClickListener(this);
        accept_pay.setOnClickListener(this);
        new_sale.setOnClickListener(this);
        my_info.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_home_img_logo);
        ImageView img2 = base_view.findViewById(R.id.frag_home_img_m_info);
        ImageView img3 = base_view.findViewById(R.id.frag_home_img_a_pay);
        ImageView img4 = base_view.findViewById(R.id.frag_home_img_n_sale);
        ImageView img5 = base_view.findViewById(R.id.frag_home_img_v_app);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_logo_no_border);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_info);
        Utility.loadImage(getActivity(), img3, R.drawable.ic_pay);
        Utility.loadImage(getActivity(), img4, R.drawable.ic_transaction);
        Utility.loadImage(getActivity(), img5, R.drawable.ic_preview);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Home");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_home_lin_lay_view_apps:
                if (homeActivity != null) {
                    MyApplicationsSearchFragment fragment = new MyApplicationsSearchFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;

            case R.id.frag_home_lin_lay_accept_pay:
                if (homeActivity != null) {
                    AcceptPaymentFragment fragment = new AcceptPaymentFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;

            case R.id.frag_home_lin_lay_my_info:
                if (homeActivity != null) {
                    MyInfoFragment fragment = new MyInfoFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }

}
