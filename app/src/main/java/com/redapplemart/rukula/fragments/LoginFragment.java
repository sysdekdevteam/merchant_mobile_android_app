package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.JustPayTransaction;
import com.redapplemart.rukula.pojo.Login;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener
        , RetrofitResponseInterface<Object> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_USER_LOGIN = 0;
    private static final int RETROFIT_REMOVE_PREVIOUS_USER_FROM_LANKAPAY = 1;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private EditText mMerchantIDEdtxt;
    private EditText mPasswordEdtxt;

    private Login mCurrentLoginResponse;


    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_login, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        if (getActivity() instanceof HomeActivity) {
            ((HomeActivity) getActivity()).setTitle("Merchant App");
        }

        mMerchantIDEdtxt = base_view.findViewById(R.id.act_login_fr_edtxt_merchat_id);
        mPasswordEdtxt = base_view.findViewById(R.id.act_login_fr_edtxt_password);

        FrameLayout login_btn = base_view.findViewById(R.id.act_login_fr_lay_login_btn);
        TextView login_txt = base_view.findViewById(R.id.act_login_fr_lay_login_btn_txt);
        login_btn.setOnClickListener(this);
        login_txt.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.act_login_fr_img1);
        ImageView img2 = base_view.findViewById(R.id.act_login_fr_img2);
        ImageView img3 = base_view.findViewById(R.id.act_login_fr_img3);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_logo_no_border);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_warn);
        Utility.loadImage(getActivity(), img3, R.drawable.ic_add_user);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Merchant App");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.theme_color));
            }

            if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.INTERNET)) {
                    Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                            PERMISSION_REQUEST_DEVICE_ID);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.act_login_fr_lay_login_btn:
                loginUser();
                break;
            case R.id.act_login_fr_lay_login_btn_txt:
                loginUser();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {

        switch (request_id) {

            case RETROFIT_REQUEST_USER_LOGIN:

                try {

                    if (response != null && (response instanceof Login) && ((Login) response).getToken() != null
                            && !((Login) response).getToken().isEmpty()) {

                        mCurrentLoginResponse = (Login) response;
                        //remove previous user details from lanak pay db
                        removePreviousUser();

                    } else {
                        if (mProgressDialog != null) mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "login failed. please try again with correct details.", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

            case RETROFIT_REMOVE_PREVIOUS_USER_FROM_LANKAPAY:
                if (mProgressDialog != null) mProgressDialog.dismiss();

                try {

                    if (response != null && (response instanceof APIResponse) && !Utility.isEmpty(((APIResponse) response).getResponseCode())
                            && (((APIResponse) response).getResponseCode().equals("0000") || ((APIResponse) response).getResponseCode().equals("1017")) && mCurrentLoginResponse != null) {

                        //process login response for proceed to next step
                        processLoginResponse(mCurrentLoginResponse);

                    } else {
                        Toast.makeText(getActivity(), "login failed. please try again with correct details.", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "login failed. please try again with correct details.", Toast.LENGTH_LONG).show();
    }


    //login user
    private void loginUser() {

        try {

            if (mMerchantIDEdtxt.getText() == null || mPasswordEdtxt.getText() == null
                    || mMerchantIDEdtxt.getText().toString().isEmpty() || mPasswordEdtxt.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "please enter valid details and try.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Login login = new Login();
            login.setMerchant_id(mMerchantIDEdtxt.getText().toString());
            login.setPassword(mPasswordEdtxt.getText().toString());
            login.setDevice_id(telephonyManager.getDeviceId());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.userLogin(login), this,
                    RETROFIT_REQUEST_USER_LOGIN);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }

    /**
     * remove previous user from lanka pay db
     **/
    private void removePreviousUser() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getJustPayCustomerToken();
            String account_ref = session.getJustPayAccountReference();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)
                    || Utility.isEmpty(account_ref) || mMerchantIDEdtxt.getText() == null || Utility.isEmpty(mMerchantIDEdtxt.getText().toString())
                    || merchant_id.equals(mMerchantIDEdtxt.getText().toString())) {
                //process login response for proceed to next step
                processLoginResponse(mCurrentLoginResponse);
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            JustPayTransaction transaction = new JustPayTransaction();
            transaction.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            transaction.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            transaction.setReferenceId(String.valueOf(System.currentTimeMillis()));
            transaction.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            transaction.setCustomerToken(customer_token);
            transaction.setAccountRef(account_ref);

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.removePreviousUser(transaction), this,
                    RETROFIT_REMOVE_PREVIOUS_USER_FROM_LANKAPAY);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }

    /**
     * process login response
     **/
    private void processLoginResponse(Login response) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            switch (response.getStatus()) {

                case Constants.API_RESPONSE_PIN_BLOCKED:
                    Toast.makeText(getActivity(), "max attempts reached. please contact your system admin to continue.", Toast.LENGTH_LONG).show();
                    break;

                case Constants.API_RESPONSE_PIN_INCORRECT:
                    Toast.makeText(getActivity(), "login failed. please try again with correct details.", Toast.LENGTH_LONG).show();
                    break;

                case Constants.API_RESPONSE_PIN_SUCCESS:

                    //if user logs out, go to enter pin page
                    if ((!Utility.isEmpty(SessionManager.getInstance(getActivity()).getPIN())
                            && SessionManager.getInstance(getActivity()).getPIN().equals(Constants.SHARED_PREFERENCES_PIN_USER_LOGS_OUT))
                            && homeActivity != null) {
                        homeActivity.clearBackStack();
                        PinEnterFragment pin_fragment = new PinEnterFragment();
                        homeActivity.replaceFragment(pin_fragment);

                        //save user token
                        mSessionManager.saveCustomerToken(response.getToken());
                        mSessionManager.saveMerchantID(mMerchantIDEdtxt.getText().toString());
                        return;
                    }

                    //save user token
                    mSessionManager.saveCustomerToken(response.getToken());
                    mSessionManager.saveMerchantID(mMerchantIDEdtxt.getText().toString());

                    if (homeActivity != null) {
                        CreatePinFragment fragment = new CreatePinFragment();
                        homeActivity.replaceFragment(fragment);
                    }

                    break;

                case Constants.API_RESPONSE_SUCCESS_AND_PIN_CONFIGURED:

                    //save user token
                    mSessionManager.saveCustomerToken(response.getToken());
                    mSessionManager.saveMerchantID(mMerchantIDEdtxt.getText().toString());

                    if (homeActivity != null) {
                        PinEnterFragment fragment = new PinEnterFragment();
                        homeActivity.replaceFragment(fragment);
                    }

                    break;

                default:
                    Toast.makeText(getActivity(), "login failed. please try again with correct details.", Toast.LENGTH_LONG).show();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
        }
    }

}
