package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lankaclear.justpay.LCTrustedSDK;
import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MobileAndNICFragment extends Fragment implements View.OnClickListener {

    public boolean isPinRecreate = false;

    //just pay mobile SDK instance
    private LCTrustedSDK mLCTrustedSDK;

    public MobileAndNICFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View base_view = inflater.inflate(R.layout.fragment_mobile_and_nic, container, false);

        TextView submit = base_view.findViewById(R.id.frag_mobile_and_nic_txt_submit);
        submit.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Mobile Number & NIC");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mLCTrustedSDK = new LCTrustedSDK(getActivity());
            mLCTrustedSDK.init();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.frag_mobile_and_nic_txt_submit) {

            try {

                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                if (isPinRecreate) {
                    if (homeActivity != null) {
                        HomeFragment fragment = new HomeFragment();
                        homeActivity.clearBackStack();
                        homeActivity.replaceFragment(fragment);
                    }
                    return;
                }

                //if user already registered a bank account, let him access without redirecting to register bank fragment
                SessionManager manager = SessionManager.getInstance(getActivity());
                if (manager != null && Utility.isEmpty(manager.getCustomerAccNo())
                        && manager.getUserChallengePassword() != null && manager.getUserChallengePassword().isEmpty()
                        && mLCTrustedSDK != null && mLCTrustedSDK.isIdentityExist()) {

                    if (homeActivity != null) {
                        HomeFragment fragment = new HomeFragment();
                        homeActivity.clearBackStack();
                        homeActivity.replaceFragment(fragment);
                    }

                    return;
                }

                if (homeActivity != null) {
                    RegisterBankAccFragment fragment = new RegisterBankAccFragment();
                    homeActivity.clearBackStack();
                    homeActivity.replaceFragment(fragment);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
