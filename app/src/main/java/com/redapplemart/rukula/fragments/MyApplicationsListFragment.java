package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.adapters.MyApplicationsListAdapter;
import com.redapplemart.rukula.consts.ApplicationsLookupSort;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.consts.TransactionsLookupSort;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.popup.MyApplicationsSortPopup;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyApplicationsListFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Customer>
        , MyApplicationsSortPopup.ApplicationSortPopupInterface {
    private static final int RETROFIT_REQUEST_ALL_CUSTOMERS = 0;

    public String mSearchCondition;

    private ProgressDialog mProgressDialog;

    private ListView mApplicationsListView;
    private MyApplicationsListAdapter mApplicationListAdapter;

    private ArrayList<Customer> mAllApplications;

    public MyApplicationsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_applications_list, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mApplicationsListView = base_view.findViewById(R.id.frag_my_applications_list_list_view);

        TextView back = base_view.findViewById(R.id.frag_my_applications_list_txtview_back);
        back.setOnClickListener(this);

        TextView sort = base_view.findViewById(R.id.frag_my_applications_list_txtview_sort);
        sort.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Search Applications");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            requestAllCustomers(ApplicationsLookupSort.SORT_NONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_my_applications_list_txtview_back:
                if (homeActivity != null) homeActivity.goBack();
                break;

            case R.id.frag_my_applications_list_txtview_sort:
                MyApplicationsSortPopup sort = new MyApplicationsSortPopup(getActivity());
                sort.setPopupActionListener(this);
                sort.show();
                break;
        }
    }


    @Override
    public void onRetrofitSuccess(Call<Customer> call, Customer response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_ALL_CUSTOMERS:

                try {

                    if (response != null && response.getItems() != null) {

                        //if multiple items available
                        if (response.getItems().size() > 0 && getActivity() != null) {

                            mAllApplications = response.getItems();

                            //create list adapter and add to list view
                            mApplicationListAdapter = new MyApplicationsListAdapter(getActivity()
                                    , R.layout.layout_my_applications_list_item, mAllApplications);
                            mApplicationsListView.setAdapter(mApplicationListAdapter);
                            mApplicationListAdapter.notifyDataSetChanged();

                            return;
                        }

                    }

                    showErrorAndGoBack(true);

                } catch (Exception e) {
                    e.printStackTrace();
                    showErrorAndGoBack(true);
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Customer> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showErrorAndGoBack(true);
    }


    @Override
    public void onSortSelected(ApplicationsLookupSort sorting) {
        requestAllCustomers(sorting);
    }


    /**
     * show error and go back
     **/
    private void showErrorAndGoBack(boolean shouldShowError) {
        if (shouldShowError)
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null) {
            homeActivity.goBack();
        }
    }

    /**
     * check for all available customers
     *
     * @param sort current search query's search options
     **/
    private void requestAllCustomers(ApplicationsLookupSort sort) {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)
                    || Utility.isEmpty(mSearchCondition)) {
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", SessionManager.getInstance(getActivity()).getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("nic_or_name", mSearchCondition);
            transaction_req.put("page", 1);
            transaction_req.put("per_page", 100);
            transaction_req.put("sort_field", getSortField(sort));
            transaction_req.put("sort_order", getSortType(sort));

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getAllAvailableCustomers(header, transaction_req), this,
                    RETROFIT_REQUEST_ALL_CUSTOMERS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }

    private String getSortField(ApplicationsLookupSort sort) {
        switch (sort) {
            case APPLICATION_ID_ASC:
                return "application_id";
            case APPLICATION_ID_DESC:
                return "application_id";
            case APPLICATION_STATUS_ASC:
                return "status";
            case APPLICATION_STATUS_DESC:
                return "status";
            case INSTALLMENT_VALUE_ASC:
                return "installment_value";
            case INSTALLMENT_VALUE_DESC:
                return "installment_value";
            case LATEST_APPLICATION_ASC:
                return "application_id";
            case LATEST_APPLICATION_DESC:
                return "application_id";
        }

        return "";
    }

    private String getSortType(ApplicationsLookupSort sort) {
        if (sort == ApplicationsLookupSort.APPLICATION_ID_ASC || sort == ApplicationsLookupSort.APPLICATION_STATUS_ASC
                || sort == ApplicationsLookupSort.INSTALLMENT_VALUE_ASC || sort == ApplicationsLookupSort.LATEST_APPLICATION_ASC) {
            return "asc";
        }

        return "desc";
    }
}
