package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyApplicationsSearchFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Customer> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS = 0;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private EditText mApplicationSearchEdtTxt;

    public MyApplicationsSearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_applications_search, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        TextView back = base_view.findViewById(R.id.frag_my_applications_search_back_txt_btn);
        TextView submit = base_view.findViewById(R.id.frag_my_applications_search_search_txt_btn);
        back.setOnClickListener(this);
        submit.setOnClickListener(this);

        mApplicationSearchEdtTxt = base_view.findViewById(R.id.frag_my_applications_search_edt_txt_query);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Application Search");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_my_applications_search_back_txt_btn:
                if (homeActivity != null) homeActivity.goBack();
                break;
            case R.id.frag_my_applications_search_search_txt_btn:

                if (mApplicationSearchEdtTxt.getText() == null || Utility.isEmpty(mApplicationSearchEdtTxt.getText().toString())) {
                    Toast.makeText(getActivity(), "Invalid search query. Please enter valid search query and retry.", Toast.LENGTH_LONG).show();
                    return;
                }

                //searchForUser();
                if (homeActivity != null) {
                    MyApplicationsListFragment fragment = new MyApplicationsListFragment();
                    fragment.mSearchCondition = mApplicationSearchEdtTxt.getText().toString();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Customer> call, Customer response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        if (response == null) {
            Toast.makeText(getActivity(), "Customer not found. Please retry.", Toast.LENGTH_LONG).show();
            return;
        }

        switch (request_id) {

            case RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS:

                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                //check for not registered customers
                if (response.get_meta() != null) {

                    if (homeActivity != null) {
                        MyApplicationsListFragment fragment = new MyApplicationsListFragment();
                        fragment.mSearchCondition = mApplicationSearchEdtTxt.getText().toString();
                        homeActivity.replaceFragment(fragment);
                    }

                } else {

                    Toast.makeText(getActivity(), "No customers available. Please enter a valid search condition and continue.", Toast.LENGTH_LONG).show();

                }

                break;

        }
    }

    @Override
    public void onRetrofitFailed(Call<Customer> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "Customer not found. Please retry.", Toast.LENGTH_LONG).show();
    }


    /**
     * search for a user with given NIC
     **/
    private void searchForUser() {

        try {

            if (mSessionManager == null)
                mSessionManager = SessionManager.getInstance(getActivity());

            if (mApplicationSearchEdtTxt.getText() == null || mApplicationSearchEdtTxt.getText().toString().isEmpty() || Utility.isEmpty(mSessionManager.getMerchantID())
                    || Utility.isEmpty(mSessionManager.getCustomerToken())) {
                Toast.makeText(getActivity(), "Applications not available for this search.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", mSessionManager.getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("nic_or_name", mApplicationSearchEdtTxt.getText().toString());
            transaction_req.put("page", 1);
            transaction_req.put("per_page", 100);

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getAllAvailableCustomers(header, transaction_req), this,
                    RETROFIT_REQUEST_CUSTOMER_SEARCH_RESULTS);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Applications not available for this search.", Toast.LENGTH_LONG).show();
            mProgressDialog.dismiss();
        }

    }

}
