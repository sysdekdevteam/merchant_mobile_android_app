package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Merchant;
import com.redapplemart.rukula.pojo.UserBankAccount;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBankAccountFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Object> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA = 0;

    private ProgressDialog mProgressDialog;

    private String mCurrentBankID, mCurrentBranchID;
    private TextView mAccountNameTxt, mAccountNoTxt, mBankTxt, mBranchTxt;

    public MyBankAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_bank_account, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        mAccountNameTxt = base_view.findViewById(R.id.frag_my_bank_account_txt_acc_name);
        mAccountNoTxt = base_view.findViewById(R.id.frag_my_bank_account_txt_acc_no);
        mBankTxt = base_view.findViewById(R.id.frag_my_bank_account_txt_bank);
        mBranchTxt = base_view.findViewById(R.id.frag_my_bank_account_txt_branch);

        TextView change_acc = base_view.findViewById(R.id.frag_my_bank_account_txt_change_acc);
        TextView back = base_view.findViewById(R.id.frag_my_bank_account_txt_back);
        change_acc.setOnClickListener(this);
        back.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //start receiving user's bank details
        getUserRegisteredBankAccounts();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Bank Account");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_my_bank_account_txt_back:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;
            case R.id.frag_my_bank_account_txt_change_acc:
                if (homeActivity != null) {
                    ChangeBankAccConfirmFragment fragment = new ChangeBankAccConfirmFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (request_id) {

            case RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA:

                try {

                    // TODO: 8/20/2018 remove local session value check, after fix the API call
                    if (response == null || !(response instanceof UserBankAccount)
                            || Utility.isEmpty(((UserBankAccount) response).getBank())
                            || Utility.isEmpty(((UserBankAccount) response).getBank())
                            || Utility.isEmpty(((UserBankAccount) response).getBranch_name())) {
                        showErrorAndGoBack();
                        return;
                    }

                    Merchant merchant = null;
                    if (homeActivity != null && homeActivity.getCurrentMerchantData() != null) {
                        merchant = homeActivity.getCurrentMerchantData();
                    }

                    UserBankAccount account = (UserBankAccount) response;
                    mAccountNameTxt.setText((merchant == null) ? "" : merchant.getFull_name());
                    mAccountNoTxt.setText(String.format(Locale.ENGLISH, "XXXX XXXX %s", account.getAccount_last_four_digits()));
                    mBankTxt.setText(account.getBank());
                    mBranchTxt.setText(account.getBranch_name());

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

        }
    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showErrorAndGoBack();
    }

    /**
     * get user registered bank account details
     **/
    private void getUserRegisteredBankAccounts() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(token)) {
                Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> user_data = new HashMap<>();
            user_data.put("merchant_id", merchant_id);
            user_data.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getUserBankDetails(header, user_data), this,
                    RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();

            Toast.makeText(getActivity(), "Error occurred. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * show error and go back
     **/
    private void showErrorAndGoBack() {
        Toast.makeText(getActivity(), "Error occurred. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) homeActivity.goBack();
    }
}
