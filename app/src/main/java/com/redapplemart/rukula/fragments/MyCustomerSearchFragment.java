package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCustomerSearchFragment extends Fragment implements View.OnClickListener {

    private TextView mSearchConditionEdtTxt;

    public MyCustomerSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_customer_search, container, false);

        mSearchConditionEdtTxt = base_view.findViewById(R.id.frag_my_customer_search_nic_nic_edt_txt);

        TextView search = base_view.findViewById(R.id.frag_my_customer_search_nic_search_txt_btn);
        TextView back = base_view.findViewById(R.id.frag_my_customer_search_nic_back_txt_btn);

        search.setOnClickListener(this);
        back.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {

            case R.id.frag_my_customer_search_nic_back_txt_btn:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;

            case R.id.frag_my_customer_search_nic_search_txt_btn:
                if (mSearchConditionEdtTxt.getText() == null || Utility.isEmpty(mSearchConditionEdtTxt.getText().toString())) {
                    Toast.makeText(getActivity(), "Please enter a valid search condition and continue.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (homeActivity != null) {
                    CustomerListFragment fragment = new CustomerListFragment();
                    fragment.mSearchCondition = mSearchConditionEdtTxt.getText().toString();
                    homeActivity.replaceFragment(fragment);
                }
                break;

        }
    }
}
