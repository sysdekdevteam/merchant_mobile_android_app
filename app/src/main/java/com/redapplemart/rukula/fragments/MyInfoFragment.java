package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyInfoFragment extends Fragment implements View.OnClickListener {


    public MyInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_info, container, false);

        LinearLayout view_cus = base_view.findViewById(R.id.frag_my_info_lin_lay_view_customers);
        LinearLayout view_bank = base_view.findViewById(R.id.frag_my_info_lin_lay_view_bank_accounts);
        LinearLayout view_transactions = base_view.findViewById(R.id.frag_my_info_lin_lay_transactions);
        TextView back = base_view.findViewById(R.id.frag_my_info_txt_back_btn);

        view_cus.setOnClickListener(this);
        view_bank.setOnClickListener(this);
        view_transactions.setOnClickListener(this);
        back.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_my_info_img1);
        ImageView img2 = base_view.findViewById(R.id.frag_my_info_img2);
        ImageView img3 = base_view.findViewById(R.id.frag_my_info_img3);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_customers);
        Utility.loadImage(getActivity(), img2, R.drawable.ic_my_bank_acc);
        Utility.loadImage(getActivity(), img3, R.drawable.ic_my_transactions);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.setTitle("My Information");
            homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {

            case R.id.frag_my_info_txt_back_btn:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;

            case R.id.frag_my_info_lin_lay_view_customers:
                if (homeActivity != null) {
                    CustomerSearchNICFragment fragment = new CustomerSearchNICFragment();
                    fragment.isCustomerSearch = true;
                    homeActivity.replaceFragment(fragment);
                }
                break;

            case R.id.frag_my_info_lin_lay_view_bank_accounts:
                if (homeActivity != null) {
                    MyBankAccountFragment fragment = new MyBankAccountFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;

            case R.id.frag_my_info_lin_lay_transactions:
                if (homeActivity != null) {
                    MyTrasactionsSearchFragment fragment = new MyTrasactionsSearchFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;

        }
    }
}
