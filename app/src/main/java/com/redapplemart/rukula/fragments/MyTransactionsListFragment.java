package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.adapters.MyTransactionsListAdapter;
import com.redapplemart.rukula.consts.ApplicationsLookupSort;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.consts.TransactionsLookupSort;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Transaction;
import com.redapplemart.rukula.popup.TransactionsSortPopup;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyTransactionsListFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Transaction>,
        TransactionsSortPopup.TransactionsSortPopupInterface {
    private static final int RETROFIT_REQUEST_ALL_CUSTOMERS = 0;

    public String mSearchTerm;

    private ProgressDialog mProgressDialog;

    private ListView mTransactionsListView;

    private MyTransactionsListAdapter mTransactionsListAdapter;
    private ArrayList<Transaction> mAllTransactions;

    public MyTransactionsListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_transactions_list, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mTransactionsListView = base_view.findViewById(R.id.frag_my_transactions_list_list_view);

        TextView back = base_view.findViewById(R.id.frag_my_transactions_list_txtview_back);
        back.setOnClickListener(this);

        TextView sort = base_view.findViewById(R.id.frag_my_transactions_list_txtview_sort);
        sort.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            //if search term is empty, redirect to previous fragment
            if (homeActivity != null && Utility.isEmpty(mSearchTerm)) {
                homeActivity.goBack();
                return;
            }

            if (homeActivity != null) {
                homeActivity.setTitle("My Transactions");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            //get all transactions from server
            requestAllTransactions(TransactionsLookupSort.SORT_NONE);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_my_transactions_list_txtview_back:
                if (homeActivity != null) homeActivity.goBack();
                break;

            case R.id.frag_my_transactions_list_txtview_sort:
                TransactionsSortPopup sort = new TransactionsSortPopup(getActivity());
                sort.setPopupActionListener(this);
                sort.show();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Transaction> call, Transaction response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_ALL_CUSTOMERS:

                try {

                    if (response != null && response.getItems() != null) {

                        //if multiple items available
                        if (response.getItems().size() > 0 && getActivity() != null) {

                            mAllTransactions = response.getItems();

                            //create list adapter and add to list view
                            mTransactionsListAdapter = new MyTransactionsListAdapter(getActivity(), R.layout.layout_my_transactions_list_item
                                    , response.getItems());
                            mTransactionsListView.setAdapter(mTransactionsListAdapter);
                            mTransactionsListAdapter.notifyDataSetChanged();

                            return;
                        }

                    }

                    showErrorAndGoBack(true);

                } catch (Exception e) {
                    e.printStackTrace();
                    showErrorAndGoBack(true);
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Transaction> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showErrorAndGoBack(true);
    }


    @Override
    public void onSortSelected(TransactionsLookupSort sorting) {
        //get all transactions from server
        requestAllTransactions(sorting);
    }


    /**
     * show error and go back
     **/
    private void showErrorAndGoBack(boolean shouldShowError) {
        if (shouldShowError)
            Toast.makeText(getActivity(), "No transactions available. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null) {
            homeActivity.goBack();
        }
    }

    /**
     * request for all my transactions
     **/
    private void requestAllTransactions(TransactionsLookupSort sort) {

        try {
            if (Utility.isEmpty(mSearchTerm)) {
                Toast.makeText(getActivity(), "Invalid search data. Please try again with valid data.", Toast.LENGTH_LONG).show();
                return;
            }

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)) {
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", SessionManager.getInstance(getActivity()).getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());
            transaction_req.put("search_text", mSearchTerm);
            transaction_req.put("page", 1);
            transaction_req.put("per_page", 100);
            transaction_req.put("sort_field", getSortField(sort));
            transaction_req.put("sort_order", getSortType(sort));

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getAllMyTransactions(header, transaction_req), this,
                    RETROFIT_REQUEST_ALL_CUSTOMERS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No transactions available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }

    private String getSortField(TransactionsLookupSort sort) {
        switch (sort) {
            case CUSTOMER_NAME_ASC:
                return "merchant_name";
            case CUSTOMER_NAME_DESC:
                return "merchant_name";
            case NIC_ASC:
                return "";
            case NIC_DESC:
                return "";
            case PAY_DATE_ASC:
                return "date_and_time";
            case PAY_DATE_DESC:
                return "date_and_time";
        }

        return "";
    }

    private String getSortType(TransactionsLookupSort sort) {
        if (sort == TransactionsLookupSort.CUSTOMER_NAME_ASC || sort == TransactionsLookupSort.NIC_ASC
                || sort == TransactionsLookupSort.PAY_DATE_ASC) {
            return "asc";
        }

        return "desc";
    }

}
