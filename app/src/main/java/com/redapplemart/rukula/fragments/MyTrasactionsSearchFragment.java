package com.redapplemart.rukula.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyTrasactionsSearchFragment extends Fragment implements View.OnClickListener {

    private EditText mSearchEdtTxt;

    public MyTrasactionsSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_my_trasactions_search, container, false);

        mSearchEdtTxt = base_view.findViewById(R.id.frag_my_transactions_search_edt_txt_query);

        TextView back_btn = base_view.findViewById(R.id.frag_my_transactions_search_back_txt_btn);
        TextView search_btn = base_view.findViewById(R.id.frag_my_transactions_search_search_txt_btn);

        back_btn.setOnClickListener(this);
        search_btn.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("My Transactions");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_my_transactions_search_back_txt_btn:
                if (homeActivity != null) homeActivity.goBack();
                break;

            case R.id.frag_my_transactions_search_search_txt_btn:
                if (mSearchEdtTxt.getText() == null || Utility.isEmpty(mSearchEdtTxt.getText().toString())) {
                    Toast.makeText(getActivity(), "Invalid search data. Please try again with valid data.", Toast.LENGTH_LONG).show();
                    return;
                }

                if (homeActivity != null) {
                    MyTransactionsListFragment fragment = new MyTransactionsListFragment();
                    fragment.mSearchTerm = mSearchEdtTxt.getText().toString();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }

    }
}
