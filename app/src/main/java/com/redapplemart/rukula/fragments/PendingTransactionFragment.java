package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.JustPayTransaction;
import com.redapplemart.rukula.pojo.PendingTransactions;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingTransactionFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Object> {
    private static final int RETROFIT_REQUEST_SELECTED_PENDING_TRANSACTION = 0;
    private static final int RETROFIT_REQUEST_ACCEPT_PENDING_TRANSACTION = 1;
    private static final int RETROFIT_REQUEST_REJECT_PENDING_TRANSACTION = 2;
    private static final int RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH = 3;

    //request current transaction id from the invoker
    public int mSelectedTransactionID = -1;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    //count down timer data
    private long mMillisUntilFinished;

    private TextView TransactionAmountTxt, TransactionCusNameTxt, TransactionMerchIDTxt;

    private TextView mTimeOutTxt;

    private PendingTransactions mCurrentTransactionDetails;

    public PendingTransactionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_pending_transaction, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        TransactionAmountTxt = base_view.findViewById(R.id.frag_pending_transact_txt_amount);
        TransactionCusNameTxt = base_view.findViewById(R.id.frag_pending_transact_txt_cus_name);
        TransactionMerchIDTxt = base_view.findViewById(R.id.frag_pending_transact_txt_merchant_id);
        mTimeOutTxt = base_view.findViewById(R.id.frag_pending_transact_txt_timeout);

        TextView accept = base_view.findViewById(R.id.frag_pending_transact_txt_accept);
        TextView reject = base_view.findViewById(R.id.frag_pending_transact_txt_decline);
        TextView back = base_view.findViewById(R.id.frag_pending_transact_txt_back);
        accept.setOnClickListener(this);
        reject.setOnClickListener(this);
        back.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_pending_transact_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_pay);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Pending Transactions");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            showTimerDetails();//show count down timer

            //check for valid transaction id
            if (mSelectedTransactionID == -1) {
                if (homeActivity != null) homeActivity.clearBackStackWithSteps(1);
                return;
            }
            mCurrentTransactionDetails = null;

            //request for selected transaction details
            requestForSelectedPendingTransaction();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMillisUntilFinished = 0;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        mMillisUntilFinished = 0;

        switch (id) {
            case R.id.frag_pending_transact_txt_decline:
                acceptOrDeclineTransaction(false);//reject transaction
                break;

            case R.id.frag_pending_transact_txt_accept:
                deductAmountFromMerchantAccount();//deduct amount from merchant's account
                break;

            case R.id.frag_pending_transact_txt_back:
                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                if (homeActivity != null) homeActivity.goBack();
                break;
        }
    }


    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {

        switch (request_id) {

            case RETROFIT_REQUEST_SELECTED_PENDING_TRANSACTION:
                if (mProgressDialog != null) mProgressDialog.dismiss();

                try {

                    if (response != null && (response instanceof PendingTransactions)) {

                        mCurrentTransactionDetails = (PendingTransactions) response;

                        TransactionAmountTxt.setText(String.format("RS. %s", mCurrentTransactionDetails.getAmount()));
                        TransactionCusNameTxt.setText(mCurrentTransactionDetails.getName());
                        TransactionMerchIDTxt.setText(mCurrentTransactionDetails.getNic());

                        return;

                    }

                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
                }

                break;

            case RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH:

                try {

                    if (response == null || !(response instanceof APIResponse) || Utility.isEmpty(((APIResponse) response).getResponseCode()) || getActivity() == null) {
                        if (mProgressDialog != null) mProgressDialog.dismiss();
                        Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    APIResponse api_response = (APIResponse) response;

                    if (!api_response.getResponseCode().equals("0000") && !Utility.isEmpty(api_response.getResponseDesc())) {
                        if (mProgressDialog != null) mProgressDialog.dismiss();
//                        Toast.makeText(getActivity(), api_response.getResponseDesc() + " -- " + api_response.getResponseCode() + " -- " + api_response.getLCComment()
//                                , Toast.LENGTH_LONG).show();
//                        Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                        showTransactionErrorFragment();//show error fragment
                        return;
                    }

                    acceptOrDeclineTransaction(true);//accept transaction

                } catch (Exception e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
//                    Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                    showTransactionErrorFragment();//show error fragment
                }
                break;

            case RETROFIT_REQUEST_ACCEPT_PENDING_TRANSACTION:
                if (mProgressDialog != null) mProgressDialog.dismiss();

                try {
                    HomeActivity homeActivity = null;
                    if (getActivity() instanceof HomeActivity)
                        homeActivity = (HomeActivity) getActivity();

                    if (response == null || !(response instanceof PendingTransactions)
                            || ((PendingTransactions) response).getStatus().contains(Constants.API_RESPONSE_STRING_SUCCESS)) {
//                        Toast.makeText(getActivity(), "Transaction successfully accepted.", Toast.LENGTH_LONG).show();
//                        if (homeActivity != null) homeActivity.goBack();
                        if (homeActivity != null) {
                            RequestCustomerPaySuccessFragment fragment = new RequestCustomerPaySuccessFragment();
                            fragment.isShowingSuccess = true;
                            homeActivity.clearBackStackWithSteps(1);
                            homeActivity.replaceFragment(fragment);
                        }
                        return;
                    }

//                    Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                    showTransactionErrorFragment();//show error fragment

                } catch (Exception e) {
                    e.printStackTrace();
//                    Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                    showTransactionErrorFragment();//show error fragment
                }

                break;

            case RETROFIT_REQUEST_REJECT_PENDING_TRANSACTION:
                if (mProgressDialog != null) mProgressDialog.dismiss();

                try {
                    HomeActivity homeActivity = null;
                    if (getActivity() instanceof HomeActivity)
                        homeActivity = (HomeActivity) getActivity();

                    if (response == null || !(response instanceof PendingTransactions)
                            || ((PendingTransactions) response).getStatus().contains(Constants.API_RESPONSE_STRING_SUCCESS)) {
                        Toast.makeText(getActivity(), "Transaction successfully rejected.", Toast.LENGTH_LONG).show();
                        if (homeActivity != null) homeActivity.goBack();
                        return;
                    }

                    Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Transaction failed. Please try again.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "Error occurred. Please try again.", Toast.LENGTH_LONG).show();

    }


    /**
     * show time out timer details
     **/
    private void showTimerDetails() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;

                long elapsedHours = mMillisUntilFinished / hoursInMilli;
                mMillisUntilFinished = mMillisUntilFinished % hoursInMilli;

                long elapsedMinutes = mMillisUntilFinished / minutesInMilli;
                mMillisUntilFinished = mMillisUntilFinished % minutesInMilli;

                long elapsedSeconds = mMillisUntilFinished / secondsInMilli;

                String time = String.format(Locale.ENGLISH, "Time-out: %02d:%02d:%02d", elapsedHours
                        , elapsedMinutes, elapsedSeconds);
                mTimeOutTxt.setText(time);

                mMillisUntilFinished += 1000;

                showTimerDetails();
            }
        }, 1000);
    }

    /**
     * requested for selected pending transaction data
     **/
    private void requestForSelectedPendingTransaction() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getSelectedPendingTransactions(header, mSelectedTransactionID), this,
                    RETROFIT_REQUEST_SELECTED_PENDING_TRANSACTION);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
//            Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
            showTransactionErrorFragment();//show error fragment
        }

    }

    /**
     * proceed with the payment deduction from merchant's bank account
     **/
    private void deductAmountFromMerchantAccount() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getJustPayCustomerToken();
            String account_ref = session.getJustPayAccountReference();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token) || Utility.isEmpty(account_ref)) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            if (mCurrentTransactionDetails == null) {
                Toast.makeText(getActivity(), "Transaction details not available. Please try again.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            JustPayTransaction transaction = new JustPayTransaction();
            transaction.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            transaction.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            transaction.setSubMerchantID(null);
            transaction.setReferenceId(String.valueOf(System.currentTimeMillis()));
            transaction.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            transaction.setCustomerToken(customer_token);
            transaction.setAccountRef(account_ref);
            transaction.setTxnAmount(mCurrentTransactionDetails.getAmount());

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.makeAPayment(transaction), this,
                    RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH);

        } catch (Exception e) {
            e.printStackTrace();

            if (mProgressDialog != null) mProgressDialog.dismiss();
//            Toast.makeText(getActivity(), "Payment failed. Please try again.", Toast.LENGTH_LONG).show();
            showTransactionErrorFragment();//show error fragment
        }
    }


    /**
     * accept or decline transaction data
     *
     * @param is_accept accept or decline
     **/
    private void acceptOrDeclineTransaction(boolean is_accept) {

        try {

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            PendingTransactions transaction = new PendingTransactions();
            transaction.setToken(mSessionManager.getCustomerToken());
            transaction.setMerchant_id(mSessionManager.getMerchantID());

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());


            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            if (is_accept) {

                APIServices services = retrofit.create(APIServices.class);
                RetrofitResponseController.executeCall(services.acceptSelectedTransaction(header, mSelectedTransactionID, transaction), this,
                        RETROFIT_REQUEST_ACCEPT_PENDING_TRANSACTION);

            } else {

                APIServices services = retrofit.create(APIServices.class);
                RetrofitResponseController.executeCall(services.rejectSelectedTransaction(header, mSelectedTransactionID, transaction), this,
                        RETROFIT_REQUEST_REJECT_PENDING_TRANSACTION);

            }

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No any pending transactions available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * show transaction error fragment
     **/
    private void showTransactionErrorFragment() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                RequestCustomerPaySuccessFragment fragment = new RequestCustomerPaySuccessFragment();
                fragment.isShowingSuccess = false;
                homeActivity.clearBackStackWithSteps(1);
                homeActivity.replaceFragment(fragment);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
