package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lankaclear.justpay.LCTrustedSDK;
import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.PIN;
import com.redapplemart.rukula.pojo.UserBankAccount;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PinEnterFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Object> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_PIN_VALIDATION = 0;
    private static final int RETROFIT_REQUEST_REGISTERED_BANK_ACC = 1;

    private SessionManager mSessionManager;
    private ProgressDialog mProgressDialog;

    private EditText mPINEdtxt;

    //just pay mobile SDK instance
    private LCTrustedSDK mLCTrustedSDK;

    public PinEnterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_pin_enter, container, false);

        mSessionManager = SessionManager.getInstance(getActivity());
        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        mPINEdtxt = base_view.findViewById(R.id.pin_enter_fr_edtxt_pin);

        FrameLayout submit = base_view.findViewById(R.id.pin_enter_fr_fr_lay_submit);
        TextView forgot_pin = base_view.findViewById(R.id.pin_enter_fr_txt_forgot_pin);
        submit.setOnClickListener(this);
        forgot_pin.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("PIN Request");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mLCTrustedSDK = new LCTrustedSDK(getActivity());
            mLCTrustedSDK.init();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {

            case R.id.pin_enter_fr_fr_lay_submit:
                validatePIN();
                break;

            case R.id.pin_enter_fr_txt_forgot_pin:
                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();
                if (homeActivity != null) {
                    HelpFragment fragment = new HelpFragment();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        switch (request_id) {

            case RETROFIT_REQUEST_PIN_VALIDATION:

                try {

                    if (response == null || !(response instanceof PIN)) {
                        Toast.makeText(getActivity(), "PIN you entered is not valid. please try again with correct details.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    switch (((PIN) response).getStatus()) {

                        case Constants.API_RESPONSE_PIN_SUCCESS:
                            //get saved user bank information
                            getUserRegisteredBankAccounts();
                            break;

                        case Constants.API_RESPONSE_PIN_BLOCKED:
                            if (homeActivity != null) {
                                HelpFragment fragment = new HelpFragment();
                                homeActivity.clearBackStack();
                                homeActivity.replaceFragment(fragment);
                            }
                            break;

                        case Constants.API_RESPONSE_PIN_TEMP:
                            if (homeActivity != null) {
                                CreatePinFragment fragment = new CreatePinFragment();
                                homeActivity.clearBackStack();
                                homeActivity.replaceFragment(fragment);
                            }
                            break;

                        default:
                            Toast.makeText(getActivity(), "PIN you entered is not valid. please try again with correct details.", Toast.LENGTH_LONG).show();
                            break;

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

            case RETROFIT_REQUEST_REGISTERED_BANK_ACC:

                try {

                    //check for saved bank accounts
                    if (mSessionManager == null)
                        mSessionManager = SessionManager.getInstance(getActivity());

                    SessionManager manager = SessionManager.getInstance(getActivity());
                    //save pin details in shared preferences
                    if (manager != null) manager.savePIN(mPINEdtxt.getText().toString());

                    // TODO: 8/20/2018 remove local session value check, after fix the API call
                    if (response == null || !(response instanceof UserBankAccount)
                            || (Utility.isEmpty(((UserBankAccount) response).getAccount_reference()) && Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayAccountReference()))
                            || (Utility.isEmpty(((UserBankAccount) response).getCustomer_token())) && Utility.isEmpty(SessionManager.getInstance(getActivity()).getJustPayCustomerToken())) {

                        if (homeActivity != null) {
                            RegisterBankAccFragment fragment = new RegisterBankAccFragment();
                            homeActivity.clearBackStack();
                            homeActivity.replaceFragment(fragment);
                        }

                        return;
                    }

                    //if user paused app in middle of something, continue from there
                    if (homeActivity != null && homeActivity.getCurrentBackStackCount() > 2) {
                        homeActivity.goBack();
                        return;
                    }

                    UserBankAccount account = (UserBankAccount) response;
                    if (!Utility.isEmpty(account.getAccount_reference()))
                        SessionManager.getInstance(getActivity()).saveJustPayAccountReference(account.getAccount_reference());
                    if (!Utility.isEmpty(account.getCustomer_token()))
                        SessionManager.getInstance(getActivity()).saveJustPayCustomerToken(account.getCustomer_token());

                    if (homeActivity != null) {
                        HomeFragment fragment = new HomeFragment();
                        homeActivity.clearBackStack();
                        homeActivity.replaceFragment(fragment);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "PIN validation failed. please try again with valid details.", Toast.LENGTH_LONG).show();
    }


    //login with user created PIN
    private void validatePIN() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (mPINEdtxt.getText() == null || mPINEdtxt.getText().toString().isEmpty()) {
                Toast.makeText(getActivity(), "please enter valid details and try.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mSessionManager != null && (mSessionManager.getMerchantID() == null || mSessionManager.getMerchantID().isEmpty()
                    || mSessionManager.getCustomerToken() == null || mSessionManager.getCustomerToken().isEmpty())) {
                Toast.makeText(getActivity(), "invalid login.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) {
                    homeActivity.clearBackStack();
                    LoginFragment fragment = new LoginFragment();
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            PIN pin = new PIN();
            pin.setPin(mPINEdtxt.getText().toString());
            pin.setMerchant_id(mSessionManager.getMerchantID());
            pin.setDevice_id(telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.validatePIN(header, pin), this,
                    RETROFIT_REQUEST_PIN_VALIDATION);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "error occurred. login failed.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }

    /**
     * get user registered bank account details
     **/
    private void getUserRegisteredBankAccounts() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(token)) {
                Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> user_data = new HashMap<>();
            user_data.put("merchant_id", merchant_id);
            user_data.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", mSessionManager.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getUserBankDetails(header, user_data), this,
                    RETROFIT_REQUEST_REGISTERED_BANK_ACC);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
