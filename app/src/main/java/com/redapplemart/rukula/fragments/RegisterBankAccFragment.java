package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lankaclear.justpay.LCTrustedSDK;
import com.lankaclear.justpay.callbacks.CreateIdentityCallback;
import com.lankaclear.justpay.callbacks.SignMessageCallback;
import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.adapters.BankDetailsAdapter;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.Bank;
import com.redapplemart.rukula.pojo.CustomerConsent;
import com.redapplemart.rukula.pojo.CustomerRegistration;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterBankAccFragment<T> extends Fragment implements View.OnClickListener
        , RetrofitResponseInterface<T> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_REGISTER_ACCOUNT = 0;
    private static final int RETROFIT_REQUEST_CUSTOMER_CONSENT = 1;
    private static final int RETROFIT_REQUEST_GET_BANK_DATA = 2;

    private Spinner mBanksSpinner, mBankBranchesSpinner;
    private EditText mAccountNoEdtTxt, mAccountNameEdtTxt;

    private ArrayList<Bank> mBankList, mBankBranchList;
    private ArrayAdapter<Bank> mBankArrayAdapter, mBankBranchArrayAdapter;

    private String mBankID, mBranchID;

    private CustomerRegistration mCustomerRegObject;

    private ProgressDialog mProgressDialog;

    private String mUserAgreementDetails;

    //just pay mobile SDK instance
    private LCTrustedSDK mLCTrustedSDK;
    //customer signed consent
    private String mCustomerSignedConsent;
    //user challange
    private String mSampathAPIUserChallenge;

    public RegisterBankAccFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_register_banc_acc, container, false);

        TextView submit = base_view.findViewById(R.id.fr_reg_bank_acc_txtview_btn_label);
        submit.setOnClickListener(this);

        mCustomerRegObject = new CustomerRegistration();

        mBanksSpinner = base_view.findViewById(R.id.fr_reg_bank_acc_spinner_banks);
        mBankBranchesSpinner = base_view.findViewById(R.id.fr_reg_bank_acc_spinner_branches);
        mAccountNoEdtTxt = base_view.findViewById(R.id.fr_reg_bank_acc_edttxt_acc_no);
        mAccountNameEdtTxt = base_view.findViewById(R.id.fr_reg_bank_acc_edttxt_acc_name);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.fr_reg_bank_acc_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_bank);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Register Bank Account");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mProgressDialog = Utility.createProgressDialog(getActivity());
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);

            if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.INTERNET)) {
                    Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                            PERMISSION_REQUEST_DEVICE_ID);
                }
            }

            mLCTrustedSDK = new LCTrustedSDK(getActivity());
            mLCTrustedSDK.init();
//            mLCTrustedSDK.clearIdentity();

            //get bank and branch data from server
            getBankDetails();
            //createBankDetailsArray();
            //create spinners with items
            //createBankDetailsSpinner();
//            createBranchDetailsSpinner(null);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.fr_reg_bank_acc_txtview_btn_label:

                try {

                    String challenge = SessionManager.getInstance(getActivity()).getUserChallengePassword();

                    if (mLCTrustedSDK == null) {
                        mLCTrustedSDK = new LCTrustedSDK(getActivity());
                        mLCTrustedSDK.init();
                    }

                    //if (Utility.isEmpty(challenge) && !mLCTrustedSDK.isIdentityExist())
                    submitData();
                    //else showTermsAndRegulations();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<T> call, final T response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        if (response == null) {
            Toast.makeText(getActivity(), "Error occurred. Please try again.", Toast.LENGTH_LONG).show();
            return;
        }

        switch (request_id) {

            case RETROFIT_REQUEST_REGISTER_ACCOUNT:

                try {

                    APIResponse api_response = (APIResponse) response;

                    if (Utility.isEmpty(api_response.getResponseCode()) || getActivity() == null) {
                        Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!api_response.getResponseCode().equals("0000") && !Utility.isEmpty(api_response.getResponseDesc())) {
                        Toast.makeText(getActivity(), api_response.getResponseDesc(), Toast.LENGTH_LONG).show();
                        return;
                    }
                    processCustomerAccRegistration(api_response);

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                }

                break;

            case RETROFIT_REQUEST_CUSTOMER_CONSENT:

                try {

                    APIResponse api_response_ = (APIResponse) response;

                    if (Utility.isEmpty(api_response_.getResponseCode()) || getActivity() == null) {
                        Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!api_response_.getResponseCode().equals("0000") && !Utility.isEmpty(api_response_.getResponseDesc())) {
                        Toast.makeText(getActivity(), api_response_.getResponseDesc(), Toast.LENGTH_LONG).show();
                        return;
                    }

                    //save bank account details
                    SessionManager session = SessionManager.getInstance(getActivity());
                    if (session != null)
                        session.saveCustomerAccountNo(mAccountNoEdtTxt.getText().toString());

                    HomeActivity homeActivity = null;
                    if (getActivity() instanceof HomeActivity)
                        homeActivity = (HomeActivity) getActivity();
                    if (homeActivity != null) {
                        VerifyBankAccFragment fragment = new VerifyBankAccFragment();
                        fragment.mBankID = mBankID;
                        fragment.mBranchID = mBranchID;
                        fragment.mAccountNo = mAccountNoEdtTxt.getText().toString();
                        homeActivity.clearBackStack();
                        homeActivity.replaceFragment(fragment);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                }

                break;

            case RETROFIT_REQUEST_GET_BANK_DATA:

                try {

                    mBankList = new ArrayList<>();

                    mBankList = (ArrayList<Bank>) response;

                    //create spinners with items
                    createBankDetailsSpinner();

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<T> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
    }

    /**
     * create banks and bank branch details
     **/
    private void createBankDetailsArray() {

        mBankList = new ArrayList<>();
        ArrayList<Bank> branches = new ArrayList<>();

        Bank bank = new Bank();
        bank.setBank_code("7278");
        bank.setBank("Sampath Bank");

        Bank branch1 = new Bank();
        branch1.setBranch_code("001");
        branch1.setBranch_name("City Office");
        Bank branch2 = new Bank();
        branch2.setBranch_code("002");
        branch2.setBranch_name("Pettah");
        Bank branch3 = new Bank();
        branch3.setBranch_code("003");
        branch3.setBranch_name("Nugegoda");
        Bank branch4 = new Bank();
        branch4.setBranch_code("004");
        branch4.setBranch_name("Borella");
        Bank branch5 = new Bank();
        branch5.setBranch_code("005");
        branch5.setBranch_name("Kiribathgoda");
        Bank branch6 = new Bank();
        branch6.setBranch_code("114");
        branch6.setBranch_name("Embuldeniya");
        branches.add(branch1);
        branches.add(branch2);
        branches.add(branch3);
        branches.add(branch4);
        branches.add(branch5);
        branches.add(branch6);

        bank.setBranches(branches);


        mBankList.add(bank);

    }

    /**
     * create bank adapter and set it to spinner
     **/
    private void createBankDetailsSpinner() {

        Context context = getActivity();

        if (context == null) return;

        ArrayAdapter<Bank> bankAdapter = new BankDetailsAdapter(context, R.layout.layout_spinner_item
                , mBankList, BankDetailsAdapter.BANK_DETAILS_ADAPTER_TYPE_BANK);
        mBanksSpinner.setAdapter(bankAdapter);

//        if (mBankList.size() > 0)
//            createBranchDetailsSpinner(mBankList.get(0));//remove this after online data

        mBanksSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mBankList != null && mBankList.size() > position) {
                    mBankID = mBankList.get(position).getBank_code();
                    createBranchDetailsSpinner(mBankList.get(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    /**
     * create branch adapters and set it to spinners
     **/
    private void createBranchDetailsSpinner(Bank selected_bank) {

        Context context = getActivity();

        if (mBankBranchList == null) mBankBranchList = new ArrayList<>();
        else mBankBranchList.clear();

        if (context == null || selected_bank == null || selected_bank.getBranches() == null || selected_bank.getBranches().size() == 0)
            mBankBranchList = new ArrayList<>();
        else {
            mBankBranchList.addAll(selected_bank.getBranches());
        }

        if (mBankBranchArrayAdapter == null) {

            mBankBranchArrayAdapter = new BankDetailsAdapter(context, R.layout.layout_spinner_item
                    , mBankBranchList, BankDetailsAdapter.BANK_DETAILS_ADAPTER_TYPE_BRANCH);
            mBankBranchesSpinner.setAdapter(mBankBranchArrayAdapter);

            mBankBranchesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mBankBranchList != null && mBankBranchList.size() > 0)
                        mBranchID = mBankBranchList.get(position).getBranch_code();

                    System.out.println("------------ " + mBranchID);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        } else {

            mBankBranchArrayAdapter.notifyDataSetChanged();

        }

    }


    /**
     * create and submit customer account registration details
     **/
    private void submitData() {

        try {
            mCustomerRegObject = new CustomerRegistration();

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            String email = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getEmail() : null;
            String nic = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getNic() : null;
            String mobile = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getMobile_no() : null;

            if (getActivity() == null || Utility.isEmpty(mBankID) || Utility.isEmpty(mBranchID)
                    || mAccountNameEdtTxt.getText() == null || Utility.isEmpty(mAccountNameEdtTxt.getText().toString())
                    || mAccountNoEdtTxt.getText() == null || Utility.isEmpty(mAccountNoEdtTxt.getText().toString())
                    || Utility.isEmpty(email) || Utility.isEmpty(nic) || Utility.isEmpty(mobile)) {
                Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();
//            String device_id = telephonyManager.getDeviceId();
//            int length = device_id.length();

            mCustomerRegObject.setBankCode(mBankID);
            mCustomerRegObject.setBranchCode(mBranchID);
            mCustomerRegObject.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            mCustomerRegObject.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            mCustomerRegObject.setJustPayCode(Constants.JUST_PAY_VERIFICATION_CODE);
            mCustomerRegObject.setCustomerEmail(email);
            mCustomerRegObject.setCustomerName(mAccountNameEdtTxt.getText().toString());
            mCustomerRegObject.setAccountNumber(mAccountNoEdtTxt.getText().toString());
            mCustomerRegObject.setCustomerNic(nic);
            mCustomerRegObject.setMobileNumber(mobile);
            mCustomerRegObject.setReferenceId(String.valueOf(System.currentTimeMillis()));
            mCustomerRegObject.setPlatform("Android");
            mCustomerRegObject.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            mCustomerRegObject.setDeviceID(telephonyManager.getDeviceId());

            //if certificate already exists send type code 02 instead of 01, then user challenge will not be created
            if (mLCTrustedSDK != null && mLCTrustedSDK.isIdentityExist() && SessionManager.getInstance(getActivity()).isBankRegistrationValid())
                mCustomerRegObject.setTypeCode(Constants.CUSTOMER_REG_CODE_ACC_REG);
            else
                mCustomerRegObject.setTypeCode(Constants.CUSTOMER_REG_CODE_INITIAL_REG);

            //save reference ID
            SessionManager.getInstance(getActivity()).saveUniqueRefID(mCustomerRegObject.getReferenceId());

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.customerAccountRegistration(mCustomerRegObject), this,
                    RETROFIT_REQUEST_REGISTER_ACCOUNT);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }


    /**
     * create and show terms and regulations to user
     **/
    private void showTermsAndRegulations() {
        if (mProgressDialog != null) mProgressDialog.show();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            String nic = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getNic() : null;

            if (getActivity() == null || Utility.isEmpty(nic)) {
                Toast.makeText(getActivity(), "Registration failed. Please retry."
                        , Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
                return;
            }

            mUserAgreementDetails = String.format(Locale.ENGLISH, "Please allow the this merchant" +
                            " to deal with my Sampath bank account %s and perform necessary transactions on behalf of me (%s)."
                    , mAccountNoEdtTxt.getText().toString(), nic);

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("User Agreement");
            builder.setMessage(mUserAgreementDetails);
            builder.setCancelable(false);
            builder.setNeutralButton("I Agree", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (mLCTrustedSDK == null) {
                        mLCTrustedSDK = new LCTrustedSDK(getActivity());
                        mLCTrustedSDK.init();
                    }

                    mLCTrustedSDK.signMessage(mUserAgreementDetails, new SignMessageCallback() {
                        @Override
                        public void onSuccess(final String s, final String s1) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    try {

                                        if (s == null || s.isEmpty()/* || !s1.toLowerCase().equals("verified")*/) {
                                            if (mProgressDialog != null) mProgressDialog.dismiss();
                                            Toast.makeText(getActivity(), "Customer consent generation failed. Please try again.", Toast.LENGTH_LONG).show();
                                            return;
                                        }

                                        mCustomerSignedConsent = s;
                                        SessionManager.getInstance(getActivity()).saveCustomerConsent(s);

                                        submitUserConsent();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(getActivity(), "Customer consent generation failed. Please try again.", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }

                        @Override
                        public void onFailed(int i, String s) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (mProgressDialog != null) mProgressDialog.dismiss();
                                    mCustomerSignedConsent = null;
                                    Toast.makeText(getActivity(), "Customer consent generation failed. Please try again.", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });

                }
            });
            AlertDialog dialog = builder.show();
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Customer consent generation failed. Please try again.", Toast.LENGTH_LONG).show();
        }

    }


    /**
     * submit user consent
     **/
    private void submitUserConsent() {
        if (mProgressDialog != null) mProgressDialog.show();
        CustomerConsent consent = new CustomerConsent();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            String nic = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getNic() : null;
            String ref_id = SessionManager.getInstance(getActivity()).getUniqueRefID();

            if (getActivity() == null || Utility.isEmpty(ref_id) || Utility.isEmpty(nic)
                    || Utility.isEmpty(mCustomerSignedConsent)) {
                Toast.makeText(getActivity(), "Customer consent process failed. Please try again.", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
                return;
            }

            consent.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            consent.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            consent.setReferenceId(ref_id);
            consent.setCustomerNic(nic);
            consent.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            consent.setCustConsent(mCustomerSignedConsent);

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.customerConsentValidation(consent), this,
                    RETROFIT_REQUEST_CUSTOMER_CONSENT);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Customer consent process failed. Please try again.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }

    /**
     * get bank and branch details from server
     **/
    private void getBankDetails() {

        try {
            if (mProgressDialog != null) mProgressDialog.show();

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getBankAndBranches(), this,
                    RETROFIT_REQUEST_GET_BANK_DATA);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * process customer account registration request response
     **/
    private void processCustomerAccRegistration(APIResponse response) {

        try {

            if (Utility.isEmpty(response.getChallenge()) || getActivity() == null) {
                Toast.makeText(getActivity(), ((Utility.isEmpty(response.getResponseDesc()) ? "Registration failed. Please retry." : response.getResponseDesc()))
                        , Toast.LENGTH_LONG).show();
                return;
            }

            mSampathAPIUserChallenge = response.getChallenge();

            SessionManager.getInstance(getActivity()).saveUserChallengePassword(response.getChallenge());

            if (mProgressDialog != null) mProgressDialog.show();

            if (mLCTrustedSDK == null) {
                mLCTrustedSDK = new LCTrustedSDK(getActivity());
                mLCTrustedSDK.init();
            }

            mLCTrustedSDK.createIdentity(response.getChallenge(), new CreateIdentityCallback() {
                @Override
                public void onSuccess() {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {

                                if (mProgressDialog != null) mProgressDialog.dismiss();

                                Toast.makeText(getActivity(), "Registration successful. Identity created for this user.", Toast.LENGTH_LONG).show();
                                showTermsAndRegulations();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }

                @Override
                public void onFailed(final int i, final String s) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mProgressDialog != null) mProgressDialog.dismiss();

                            System.out.println("sdk error -- " + i + " -- " + s);
                            Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Registration failed. Please retry.", Toast.LENGTH_LONG).show();
        }

    }
}
