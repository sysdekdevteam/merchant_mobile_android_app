package com.redapplemart.rukula.fragments;


import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestCustomerInfoFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Customer> {
    private static final int RETROFIT_REQUEST_SELECTED_CUSTOMERS = 0;

    //current user searched customer details
    public Customer mCurrentSelectedCustomerData;
    public boolean isExistingCustomer;
    private String mCurrentProductNames;

    private TextView mTxtFirstName, mTxtLastName, mTxtDate, mTxtNIC, mTxtDuePay, mTxtNxtInstall, mTxtTotPending, mTxtProd;

    private TextView mTxtNEFirstName, mTxtNELastName, mTxtNEDate, mTxtNENIC;

    private LinearLayout mExistingCustomerPanel, mNonExistingCustomerpanel;

    private ProgressDialog mProgressDialog;


    public RequestCustomerInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_request_customer_info, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        mTxtFirstName = base_view.findViewById(R.id.frag_req_customer_info_txt_fn);
        mTxtLastName = base_view.findViewById(R.id.frag_req_customer_info_txt_ln);
        mTxtDate = base_view.findViewById(R.id.frag_req_customer_info_txt_date);
        mTxtNIC = base_view.findViewById(R.id.frag_req_customer_info_txt_nic);
        mTxtDuePay = base_view.findViewById(R.id.frag_req_customer_info_txt_due_payment);
        mTxtNxtInstall = base_view.findViewById(R.id.frag_req_customer_info_txt_nxt_installment);
        mTxtTotPending = base_view.findViewById(R.id.frag_req_customer_info_txt_total_pending);
        mTxtProd = base_view.findViewById(R.id.frag_req_customer_info_txt_prod);

        mTxtNEFirstName = base_view.findViewById(R.id.frag_req_customer_info_txt_fn_ne);
        mTxtNELastName = base_view.findViewById(R.id.frag_req_customer_info_txt_ln_ne);
        mTxtNEDate = base_view.findViewById(R.id.frag_req_customer_info_txt_date_ne);
        mTxtNENIC = base_view.findViewById(R.id.frag_req_customer_info_txt_nic_ne);

        mExistingCustomerPanel = base_view.findViewById(R.id.frag_req_customer_info_lin_lay_existing);
        mNonExistingCustomerpanel = base_view.findViewById(R.id.frag_req_customer_info_lin_lay_non_existing);

        TextView accept = base_view.findViewById(R.id.frag_req_cus_pay_txt_accept);
        TextView back = base_view.findViewById(R.id.frag_req_cus_pay_txt_back);
        accept.setOnClickListener(this);
        back.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mCurrentSelectedCustomerData == null) {
            showErrorAndGoBack(true);
            return;
        }

        mCurrentProductNames = mCurrentSelectedCustomerData.getProduct_name();

        //get customer details from server
        getCustomerData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_req_cus_pay_txt_back:
                if (homeActivity != null) {
                    showErrorAndGoBack(false);
                }
                break;
            case R.id.frag_req_cus_pay_txt_accept:

                if (homeActivity != null) {
                    RequestCustomerPaymentFragment fragment = new RequestCustomerPaymentFragment();
                    fragment.isExistingCustomer = this.isExistingCustomer;
                    fragment.mCurrentSelectedCustomerData = this.mCurrentSelectedCustomerData;
                    homeActivity.replaceFragment(fragment);
                }

                break;
        }
    }


    @Override
    public void onRetrofitSuccess(Call<Customer> call, Customer response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {

            case RETROFIT_REQUEST_SELECTED_CUSTOMERS:

                try {

                    if (response == null || getActivity() == null) {
                        showErrorAndGoBack(true);
                        return;
                    }

                    mCurrentSelectedCustomerData = response;
                    //set customer data to fields
                    manageCustomerDetails();


                } catch (Exception e) {
                    e.printStackTrace();
                    showErrorAndGoBack(true);
                }

                break;

        }

    }

    @Override
    public void onRetrofitFailed(Call<Customer> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showErrorAndGoBack(true);
    }


    /**
     * show error and go back
     **/
    private void showErrorAndGoBack(boolean shouldShowError) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        if (shouldShowError)
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null) {
            homeActivity.goBack();
        }
    }

    /**
     * get current selected customer details
     **/
    private void getCustomerData() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)
                    || mCurrentSelectedCustomerData == null) {
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", SessionManager.getInstance(getActivity()).getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.getSelectedCustomerDetails(header, mCurrentSelectedCustomerData.getId(), transaction_req), this,
                    RETROFIT_REQUEST_SELECTED_CUSTOMERS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * set customer details to the correct fields
     **/
    private void manageCustomerDetails() {

        try {

            if (mCurrentSelectedCustomerData == null) {
                showErrorAndGoBack(true);
                return;
            }

            String date = Utility.getCurrentDate("dd/MM/yyyy");

            if (isExistingCustomer) {
                mNonExistingCustomerpanel.setVisibility(View.GONE);
                mExistingCustomerPanel.setVisibility(View.VISIBLE);

                mTxtFirstName.setText(mCurrentSelectedCustomerData.getFirst_name());
                mTxtLastName.setText(mCurrentSelectedCustomerData.getLast_name());
                mTxtDate.setText((Utility.isEmpty(date)) ? "" : date);
                mTxtNIC.setText(mCurrentSelectedCustomerData.getNic());
                mTxtDuePay.setText(String.format(Locale.ENGLISH, "Rs. %.2f"
                        , ((Utility.isEmpty(mCurrentSelectedCustomerData.getOutstanding_as_of_today())) ? 0 : Double.valueOf(mCurrentSelectedCustomerData.getOutstanding_as_of_today()))));
                mTxtTotPending.setText(String.format(Locale.ENGLISH, "Rs. %.2f"
                        , ((Utility.isEmpty(mCurrentSelectedCustomerData.getTotal_outstanding())) ? 0 : Double.valueOf(mCurrentSelectedCustomerData.getTotal_outstanding()))));
                mTxtNxtInstall.setText(String.format(Locale.ENGLISH, "Rs. %.2f", mCurrentSelectedCustomerData.getCalculatedInstallment()));
                mTxtProd.setText(mCurrentProductNames == null ? "" : mCurrentProductNames);

            } else {
                mExistingCustomerPanel.setVisibility(View.GONE);
                mNonExistingCustomerpanel.setVisibility(View.VISIBLE);

                mTxtNEFirstName.setText(mCurrentSelectedCustomerData.getFirst_name());
                mTxtNELastName.setText(mCurrentSelectedCustomerData.getLast_name());
                mTxtNENIC.setText(mCurrentSelectedCustomerData.getNic());
                mTxtNEDate.setText(date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
