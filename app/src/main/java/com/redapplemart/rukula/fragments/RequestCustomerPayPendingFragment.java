package com.redapplemart.rukula.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestCustomerPayPendingFragment extends Fragment {

    public RequestCustomerPayPendingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_request_customer_pay_pending, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();

                if (homeActivity != null) {
                    homeActivity.goBack();
                    RequestCustomerPaySuccessFragment fragment = new RequestCustomerPaySuccessFragment();
                    homeActivity.replaceFragment(fragment);
                }
            }
        }, 1000);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
