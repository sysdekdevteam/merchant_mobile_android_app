package com.redapplemart.rukula.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestCustomerPaySuccessFragment extends Fragment implements View.OnClickListener {

    public boolean isShowingSuccess = false;

    private TextView mMsgTxt;
    private LinearLayout mMainPanel;

    public RequestCustomerPaySuccessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_request_customer_pay_success, container, false);

        mMsgTxt = base_view.findViewById(R.id.frag_request_customer_pay_success_txt_msg);
        mMainPanel = base_view.findViewById(R.id.frag_request_customer_pay_success_lin_lay_main_container);
        TextView back = base_view.findViewById(R.id.frag_req_cus_pay_success_txt_back);
        back.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mMsgTxt.setText("UNSUCCESSFUL!\nPLEASE DONT ACCEPT CASH");
        mMainPanel.setBackgroundColor(getResources().getColor(R.color.red_color_1));
        if (isShowingSuccess) {
            mMsgTxt.setText("SUCCESSFUL!\nPLEASE ACCEPT CASH");
            mMainPanel.setBackgroundColor(getResources().getColor(R.color.green_color_1));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_req_cus_pay_success_txt_back:
                if (homeActivity != null) {
                    homeActivity.clearBackStackWithSteps(1);
                    homeActivity.goBack();
                }
                break;
        }
    }
}
