package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.CommonResponse;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.pojo.JustPayTransaction;
import com.redapplemart.rukula.pojo.Transaction;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestCustomerPaymentFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Object> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_SELECTED_CUSTOMERS = 0;
    private static final int RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH = 1;
    private static final int RETROFIT_REQUEST_SAVE_TRANSACTION_DATA = 2;

    //current user searched customer details
    public Customer mCurrentSelectedCustomerData;
    public boolean isExistingCustomer;
    private String mCurrentProductNames;

    private TextView mTxtFirstName, mTxtLastName, mTxtDate, mTxtNIC, mTxtDuePay, mTxtNxtInstall, mTxtTotPending, mTxtProd;
    private EditText mEdtTxtCurrentPay;

    private TextView mTxtNEFirstName, mTxtNELastName, mTxtNEDate, mTxtNENIC;

    private LinearLayout mExistingCustomerPanel, mNonExistingCustomerpanel;
    private LinearLayout mMainPanel, mPendingPanel;

    private Transaction transaction;

    private ProgressDialog mProgressDialog;

    public RequestCustomerPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_request_customer_payment, container, false);

        mProgressDialog = Utility.createProgressDialog(getActivity());
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);

        mTxtFirstName = base_view.findViewById(R.id.frag_req_customer_payment_txt_fn);
        mTxtLastName = base_view.findViewById(R.id.frag_req_customer_payment_txt_ln);
        mTxtDate = base_view.findViewById(R.id.frag_req_customer_payment_txt_date);
        mTxtNIC = base_view.findViewById(R.id.frag_req_customer_payment_txt_nic);
        mTxtDuePay = base_view.findViewById(R.id.frag_req_customer_payment_txt_due_payment);
        mTxtNxtInstall = base_view.findViewById(R.id.frag_req_customer_payment_txt_nxt_installment);
        mTxtTotPending = base_view.findViewById(R.id.frag_req_customer_payment_txt_total_pending);
        mTxtProd = base_view.findViewById(R.id.frag_req_customer_payment_txt_prod);

        mTxtNEFirstName = base_view.findViewById(R.id.frag_req_customer_payment_txt_fn_ne);
        mTxtNELastName = base_view.findViewById(R.id.frag_req_customer_payment_txt_ln_ne);
        mTxtNEDate = base_view.findViewById(R.id.frag_req_customer_payment_txt_date_ne);
        mTxtNENIC = base_view.findViewById(R.id.frag_req_customer_payment_txt_nic_ne);

        mExistingCustomerPanel = base_view.findViewById(R.id.frag_req_customer_payment_lin_lay_existing);
        mNonExistingCustomerpanel = base_view.findViewById(R.id.frag_req_customer_payment_lin_lay_non_existing);

        mMainPanel = base_view.findViewById(R.id.frag_req_customer_payment_lin_lay_main);
        mPendingPanel = base_view.findViewById(R.id.frag_req_customer_payment_lin_lay_pending);

        mEdtTxtCurrentPay = base_view.findViewById(R.id.frag_req_customer_payment_txtedt_current_payment);

        TextView accept = base_view.findViewById(R.id.frag_req_cus_pay_txt_accept);
        TextView back = base_view.findViewById(R.id.frag_req_cus_pay_txt_back);
        accept.setOnClickListener(this);
        back.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mCurrentSelectedCustomerData == null) {
            showErrorAndGoBack(true);
            return;
        }

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Customer Information");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mCurrentProductNames = mCurrentSelectedCustomerData.getProduct_name();

            //get customer details from server
            getCustomerData();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_req_cus_pay_txt_back:
                if (homeActivity != null) {
                    homeActivity.goBack();
                }
                break;
            case R.id.frag_req_cus_pay_txt_accept:
                /*if (homeActivity != null) {
                    RequestCustomerPayPendingFragment fragment = new RequestCustomerPayPendingFragment();
                    homeActivity.replaceFragment(fragment);
                }*/

                deductAmountFromMerchantAccount();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {
            case RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH:

                try {

                    if (response == null || !(response instanceof APIResponse) || Utility.isEmpty(((APIResponse) response).getResponseCode()) || getActivity() == null) {
                        showPendingNotification(false);
                        showNotificationMsg(false);
                        return;
                    }

                    APIResponse api_response = (APIResponse) response;

                    if (!api_response.getResponseCode().equals("0000") && !Utility.isEmpty(api_response.getResponseDesc())) {
                        Toast.makeText(getActivity(), api_response.getResponseDesc() + " -- " + api_response.getResponseCode() + " -- " + api_response.getLCComment()
                                , Toast.LENGTH_LONG).show();
                        showPendingNotification(false);
                        showNotificationMsg(false);
                        return;
                    }
                    //ashain
                    transaction = new Transaction();
                    transaction.setTran_id(api_response.getResponseCode());
                    saveCurrentTransaction();//save current transaction details in rukula servers

                } catch (Exception e) {
                    e.printStackTrace();
                    showPendingNotification(false);
                    showNotificationMsg(false);
                }

                break;

            case RETROFIT_REQUEST_SAVE_TRANSACTION_DATA:

                try {
                    showPendingNotification(false);

                    if (response == null || !(response instanceof CommonResponse) || Utility.isEmpty(((CommonResponse) response).getStatus()) || getActivity() == null) {
                        showNotificationMsg(false);
                        return;
                    }

                    CommonResponse api_response = (CommonResponse) response;

                    if (!api_response.getStatus().equals(Constants.API_RESPONSE_STRING_SUCCESS)) {
                        showNotificationMsg(false);
                        return;
                    }

                    showNotificationMsg(true);

                } catch (Exception e) {
                    e.printStackTrace();
                    showNotificationMsg(false);
                }

                break;

            case RETROFIT_REQUEST_SELECTED_CUSTOMERS:

                try {

                    if (response == null || getActivity() == null || !(response instanceof Customer)) {
                        showErrorAndGoBack(true);
                        return;
                    }

                    mCurrentSelectedCustomerData = (Customer) response;

                    //set customer data to fields
                    manageCustomerDetails();

                } catch (Exception e) {
                    e.printStackTrace();
                    showErrorAndGoBack(true);
                }

                break;
        }

    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        showPendingNotification(false);
        Toast.makeText(getActivity(), "Error occurred. Please try again.", Toast.LENGTH_LONG).show();
    }


    /**
     * show notification message
     **/
    private void showNotificationMsg(boolean is_success) {

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity)
            homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.goBack();
            RequestCustomerPaySuccessFragment fragment = new RequestCustomerPaySuccessFragment();
            fragment.isShowingSuccess = is_success;
            homeActivity.replaceFragment(fragment);
        }
    }

    /**
     * show error and go back
     **/
    private void showErrorAndGoBack(boolean shouldShowError) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        if (shouldShowError)
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();
        if (homeActivity != null) {
            homeActivity.goBack();
        }
    }

    /**
     * proceed with the payment deduction from merchant's bank account
     **/
    private void deductAmountFromMerchantAccount() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getJustPayCustomerToken();
            String account_ref = session.getJustPayAccountReference();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token) || Utility.isEmpty(account_ref)
                    || mEdtTxtCurrentPay.getText() == null || Utility.isEmpty(mEdtTxtCurrentPay.getText().toString())) {
                if (homeActivity != null) {
                    RequestCustomerPaySuccessFragment fragment = new RequestCustomerPaySuccessFragment();
                    fragment.isShowingSuccess = false;
                    homeActivity.replaceFragment(fragment);
                }
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            showPendingNotification(true);

            JustPayTransaction transaction = new JustPayTransaction();
            transaction.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            transaction.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            transaction.setSubMerchantID(null);
            transaction.setReferenceId(String.valueOf(System.currentTimeMillis()));
            transaction.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            transaction.setCustomerToken(customer_token);
            transaction.setAccountRef(account_ref);
            transaction.setTxnAmount(mEdtTxtCurrentPay.getText().toString());

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.makeAPayment(transaction), this,
                    RETROFIT_REQUEST_DEDUCT_PAYMENT_WITH_SAMPATH);

        } catch (Exception e) {
            e.printStackTrace();

            showPendingNotification(false);
            showNotificationMsg(false);
        }
    }


    /**
     * save current transaction details in Rukula server db
     **/
    private void saveCurrentTransaction() {

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            String nic = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getNic() : null;

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getJustPayCustomerToken();
            String token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token) || Utility.isEmpty(nic)
                    || mEdtTxtCurrentPay.getText() == null || Utility.isEmpty(mEdtTxtCurrentPay.getText().toString()) || Utility.isEmpty(token)) {

                showPendingNotification(false);
                showNotificationMsg(false);
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();

                showPendingNotification(false);
                showNotificationMsg(false);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            showPendingNotification(true);

            String timestamp = Utility.getCurrentDate("ddMMyyyyHHmmss");

            transaction.setDevice_id(telephonyManager.getDeviceId());
            transaction.setMerchant_id(merchant_id);
            //transaction.setNic(nic);
            transaction.setNic(mCurrentSelectedCustomerData.getNic());
            transaction.setPayment_value(mEdtTxtCurrentPay.getText().toString());
            transaction.setReference_id(String.format("%s%s_%s", Constants.CURRENT_TRANSACTION_UID_PREFIX
                    , timestamp, telephonyManager.getDeviceId()));
            transaction.setTime_stamp(timestamp);

            String header = String.format(Locale.ENGLISH, "token %s", token);

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.proceedCustomerPayment(header, transaction), this,
                    RETROFIT_REQUEST_SAVE_TRANSACTION_DATA);

        } catch (Exception e) {
            e.printStackTrace();

            showPendingNotification(false);
            showNotificationMsg(false);
        }
    }

    /**
     * get current selected customer details
     **/
    private void getCustomerData() {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();
            String customer_token = session.getCustomerToken();

            if (getActivity() == null || Utility.isEmpty(merchant_id) || Utility.isEmpty(customer_token)
                    || mCurrentSelectedCustomerData == null) {
                Toast.makeText(getActivity(), "Invalid session", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            Map<String, Object> transaction_req = new HashMap<>();
            transaction_req.put("merchant_id", SessionManager.getInstance(getActivity()).getMerchantID());
            transaction_req.put("device_id", telephonyManager.getDeviceId());

            String header = String.format(Locale.ENGLISH, "token %s", SessionManager.getInstance(getActivity()).getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);

            //ashain
            RetrofitResponseController.executeCall(services.getSelectedCustomerDetails(header, mCurrentSelectedCustomerData.getMember_id(), transaction_req), this,
                    RETROFIT_REQUEST_SELECTED_CUSTOMERS);

        } catch (Exception e) {
            e.printStackTrace();
            if (mProgressDialog != null) mProgressDialog.dismiss();
            Toast.makeText(getActivity(), "No customers available. Please try again.", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * set customer details to the correct fields
     **/
    private void manageCustomerDetails() {

        try {

            if (mCurrentSelectedCustomerData == null) {
                showErrorAndGoBack(true);
                return;
            }

            String date = Utility.getCurrentDate("dd/MM/yyyy");

            if (isExistingCustomer) {
                mNonExistingCustomerpanel.setVisibility(View.GONE);
                mExistingCustomerPanel.setVisibility(View.VISIBLE);

                mTxtFirstName.setText(mCurrentSelectedCustomerData.getFirst_name());
                mTxtLastName.setText(mCurrentSelectedCustomerData.getLast_name());
                mTxtDate.setText((Utility.isEmpty(date)) ? "" : date);
                mTxtNIC.setText(mCurrentSelectedCustomerData.getNic());
                mTxtDuePay.setText(String.format(Locale.ENGLISH, "Rs. %.2f"
                        , ((Utility.isEmpty(mCurrentSelectedCustomerData.getOutstanding_as_of_today())) ? 0 : Double.valueOf(mCurrentSelectedCustomerData.getOutstanding_as_of_today()))));
                mTxtTotPending.setText(String.format(Locale.ENGLISH, "Rs. %.2f"
                        , ((Utility.isEmpty(mCurrentSelectedCustomerData.getTotal_outstanding())) ? 0 : Double.valueOf(mCurrentSelectedCustomerData.getTotal_outstanding()))));
                mTxtNxtInstall.setText(String.format(Locale.ENGLISH, "Rs. %.2f", mCurrentSelectedCustomerData.getCalculatedInstallment()));
                mTxtProd.setText(mCurrentProductNames == null ? "" : mCurrentProductNames);

            } else {
                mExistingCustomerPanel.setVisibility(View.GONE);
                mNonExistingCustomerpanel.setVisibility(View.VISIBLE);

                mTxtNEFirstName.setText(mCurrentSelectedCustomerData.getFirst_name());
                mTxtNELastName.setText(mCurrentSelectedCustomerData.getLast_name());
                mTxtNENIC.setText(mCurrentSelectedCustomerData.getNic());
                mTxtNEDate.setText(date);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * show or hide pending notification
     **/
    private void showPendingNotification(boolean show) {
        mMainPanel.setVisibility(show ? View.GONE : View.VISIBLE);
        mPendingPanel.setVisibility(show ? View.VISIBLE : View.GONE);
    }

}
