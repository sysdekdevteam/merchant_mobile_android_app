package com.redapplemart.rukula.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.consts.Constants;
import com.redapplemart.rukula.controllers.RetrofitController;
import com.redapplemart.rukula.controllers.RetrofitResponseController;
import com.redapplemart.rukula.interfaces.APIServices;
import com.redapplemart.rukula.interfaces.RetrofitResponseInterface;
import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.RandomAmount;
import com.redapplemart.rukula.pojo.UserBankAccount;
import com.redapplemart.rukula.util.SessionManager;
import com.redapplemart.rukula.util.Utility;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyBankAccFragment extends Fragment implements View.OnClickListener, RetrofitResponseInterface<Object> {
    private static final int PERMISSION_REQUEST_DEVICE_ID = 0;

    private static final int RETROFIT_REQUEST_AMOUNT_VALIDATION = 0;
    private static final int RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA = 1;

    public String mBankID;
    public String mBranchID;
    public String mAccountNo;

    private ProgressDialog mProgressDialog;

    private EditText mRandomAmtEdtTxt;

    public VerifyBankAccFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_verify_bank_acc, container, false);

        TextView submit = base_view.findViewById(R.id.frag_verify_bank_txt_submit);
        mRandomAmtEdtTxt = base_view.findViewById(R.id.frag_verify_bank_acc_edtxt_random_amt);

        submit.setOnClickListener(this);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

            if (homeActivity != null) {
                homeActivity.setTitle("Verify Bank Account");
                homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
            }

            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);

            if (getActivity() != null && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.READ_PHONE_STATE) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.INTERNET)) {
                    Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                } else {
                    // No explanation needed; request the permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                            PERMISSION_REQUEST_DEVICE_ID);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_DEVICE_ID && getActivity() != null && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET},
                    0);
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.frag_verify_bank_txt_submit:
                validateRandomAmountFromUser();
                break;
        }
    }

    @Override
    public void onRetrofitSuccess(Call<Object> call, Object response, int request_id) {
        if (mProgressDialog != null) mProgressDialog.dismiss();

        switch (request_id) {
            case RETROFIT_REQUEST_AMOUNT_VALIDATION:

                try {

                    if (response == null || !(response instanceof APIResponse) || Utility.isEmpty(((APIResponse) response).getResponseCode()) || getActivity() == null) {
                        Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (!((APIResponse) response).getResponseCode().equals("0000") && !Utility.isEmpty(((APIResponse) response).getResponseDesc())) {
                        Toast.makeText(getActivity(), ((APIResponse) response).getResponseDesc() + " -- " + ((APIResponse) response).getResponseCode() + " -- " + ((APIResponse) response).getLCComment()
                                , Toast.LENGTH_LONG).show();
                        return;
                    }

                    APIResponse api_response = ((APIResponse) response);

                    //save response details
                    SessionManager session = SessionManager.getInstance(getActivity());
                    session.saveJustPayCustomerToken(api_response.getCustomerToken());
                    session.saveJustPayAccountReference(api_response.getAccountReference());

                    //save registered bank details to server db
                    saveUserRegisteredBankAccount(api_response.getCustomerToken(), api_response.getAccountReference());

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                break;

            case RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA:

                if (!(response instanceof UserBankAccount)) {
                    Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
                    return;
                }

                HomeActivity homeActivity = null;
                if (getActivity() instanceof HomeActivity)
                    homeActivity = (HomeActivity) getActivity();
                if (homeActivity != null) {
                    VerifyBankSuccessFragment fragment = new VerifyBankSuccessFragment();
                    homeActivity.clearBackStack();
                    homeActivity.replaceFragment(fragment);
                }

                break;

        }
    }

    @Override
    public void onRetrofitFailed(Call<Object> call) {
        if (mProgressDialog != null) mProgressDialog.dismiss();
        Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
    }


    /**
     * validate deducted random amount from the use account
     **/
    private void validateRandomAmountFromUser() {
        RandomAmount random_amount = new RandomAmount();

        try {

            HomeActivity homeActivity = null;
            if (getActivity() instanceof HomeActivity)
                homeActivity = (HomeActivity) getActivity();

            String nic = (homeActivity != null && homeActivity.getCurrentMerchantData() != null) ? homeActivity.getCurrentMerchantData().getNic() : null;
            String ref_id = SessionManager.getInstance(getActivity()).getUniqueRefID();

            if (getActivity() == null || Utility.isEmpty(ref_id) || Utility.isEmpty(nic)) {
                Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
                if (homeActivity != null) homeActivity.goBack();
                return;
            }

            if (mRandomAmtEdtTxt.getText() == null || Utility.isEmpty(mRandomAmtEdtTxt.getText().toString())) {
                Toast.makeText(getActivity(), "Please enter the amount deducted from your account.", Toast.LENGTH_LONG).show();
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            random_amount.setMerchantID(Constants.SAMPATH_VERIFICATION_MERCHANT_ID);
            random_amount.setMerchantToken(Constants.SAMPATH_VERIFICATION_MERCHANT_TOKEN);
            random_amount.setReferenceId(ref_id);
            random_amount.setCustomerNic(nic);
            random_amount.setTimeStamp(String.valueOf(System.currentTimeMillis()));
            random_amount.setRandomAmt(mRandomAmtEdtTxt.getText().toString());

            Retrofit retrofit = RetrofitController.getInstance(Constants.BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.deductRandomAmount(random_amount), this,
                    RETROFIT_REQUEST_AMOUNT_VALIDATION);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), "Amount validation failed. Please retry.", Toast.LENGTH_LONG).show();
            if (mProgressDialog != null) mProgressDialog.dismiss();
        }

    }

    /**
     * save user registered bank account details
     **/
    private void saveUserRegisteredBankAccount(String customer_token, String account_ref) {

        try {

            SessionManager session = SessionManager.getInstance(getActivity());
            String merchant_id = session.getMerchantID();

            if (getActivity() == null || Utility.isEmpty(mBankID) || Utility.isEmpty(mBranchID)
                    || Utility.isEmpty(mAccountNo)
                    || mAccountNo.length() < 5 || Utility.isEmpty(merchant_id)) {
                Toast.makeText(getActivity(), "Data you entered is incorrect. Please try again with correct data.", Toast.LENGTH_LONG).show();
                return;
            }

            TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager == null || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Please grant permission to these actions if you wish to continue.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_DEVICE_ID);
                return;
            }

            if (mProgressDialog != null) mProgressDialog.show();

            UserBankAccount account = new UserBankAccount();
            account.setMerchant_id(merchant_id);
            account.setDevice_id(telephonyManager.getDeviceId());
            account.setBank_code(mBankID);
            account.setBranch_code(mBranchID);
            account.setAccount_last_four_digits(mAccountNo.substring(mAccountNo.length() - 4, mAccountNo.length()));
            account.setCustomer_token(customer_token);
            account.setAccount_reference(account_ref);
            account.setToken(session.getCustomerToken());

            String header = String.format(Locale.ENGLISH, "token %s", session.getCustomerToken());

            Retrofit retrofit = RetrofitController.getInstance(Constants.RUKULA_BASE_URL, getActivity()).getRetrofitEngine();
            APIServices services = retrofit.create(APIServices.class);
            RetrofitResponseController.executeCall(services.registerUserBankDetails(header, account), this,
                    RETROFIT_REQUEST_SAVE_REGISTERED_BANK_DATA);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
