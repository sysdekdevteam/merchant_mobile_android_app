package com.redapplemart.rukula.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.activities.HomeActivity;
import com.redapplemart.rukula.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyBankSuccessFragment extends Fragment implements View.OnClickListener {

    public VerifyBankSuccessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View base_view = inflater.inflate(R.layout.fragment_verify_bank_success, container, false);

        TextView submit = base_view.findViewById(R.id.frag_verify_bank_success_txt_submit);
        submit.setOnClickListener(this);

        //load images with picasso
        ImageView img1 = base_view.findViewById(R.id.frag_verify_bank_success_img1);

        Utility.loadImage(getActivity(), img1, R.drawable.ic_clap);

        return base_view;
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        if (homeActivity != null) {
            homeActivity.setTitle("Verify Bank Account");
            homeActivity.setTitleBackgroundColor(getResources().getColor(R.color.gray_color_2));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        HomeActivity homeActivity = null;
        if (getActivity() instanceof HomeActivity) homeActivity = (HomeActivity) getActivity();

        switch (id) {
            case R.id.frag_verify_bank_success_txt_submit:
                if (homeActivity != null) {
                    HomeFragment fragment = new HomeFragment();
                    homeActivity.clearBackStack();
                    homeActivity.replaceFragment(fragment);
                }
                break;
        }
    }
}
