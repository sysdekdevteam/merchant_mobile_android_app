package com.redapplemart.rukula.interfaces;


import com.redapplemart.rukula.pojo.APIResponse;
import com.redapplemart.rukula.pojo.Bank;
import com.redapplemart.rukula.pojo.CommonResponse;
import com.redapplemart.rukula.pojo.Customer;
import com.redapplemart.rukula.pojo.CustomerConsent;
import com.redapplemart.rukula.pojo.CustomerRegistration;
import com.redapplemart.rukula.pojo.JustPayTransaction;
import com.redapplemart.rukula.pojo.Login;
import com.redapplemart.rukula.pojo.Merchant;
import com.redapplemart.rukula.pojo.PIN;
import com.redapplemart.rukula.pojo.PendingTransactions;
import com.redapplemart.rukula.pojo.RandomAmount;
import com.redapplemart.rukula.pojo.Transaction;
import com.redapplemart.rukula.pojo.UserBankAccount;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by SYSDEK Pvt Ltd on 7/27/17.
 *
 * @POST creates an Http POST request
 * @GET creates an Http GET request
 * @BODY this valid only if user specify an converter in the retrofit initialization (eg: GSON converter)
 * @HEADER add custom headers to the request
 */

public interface APIServices {

    /**
     * can register a new customer to the system
     *
     * @param customerRegistration customer registration object
     */
    @POST("LowValueCEFTWeb/smb-resources/RegisterDetails")
    Call<APIResponse> customerAccountRegistration(@Body CustomerRegistration customerRegistration) throws Exception;

    /**
     * get the signed consent and sent to the bank
     *
     * @param customerConsent signed customer agreed terms and regulation data
     */
    @POST("LowValueCEFTWeb/smb-resources/GiveConsent")
    Call<APIResponse> customerConsentValidation(@Body CustomerConsent customerConsent) throws Exception;

    /**
     * deduct random amount from customer's account
     *
     * @param randomAmount deduct random amount from customer's bank account to validate the bank account
     */
    @POST("LowValueCEFTWeb/smb-resources/ValidateAmt")
    Call<APIResponse> deductRandomAmount(@Body RandomAmount randomAmount) throws Exception;

    /**
     * make payment using just pay
     *
     * @param transaction random amount transaction details
     */
    @POST("LowValueCEFTWeb/smb-resources/MakePayment")
    Call<APIResponse> makeAPayment(@Body JustPayTransaction transaction) throws Exception;

    /**
     * remove device registration for given user
     *
     * @param transaction random amount transaction details
     */
    @POST("LowValueCEFTWeb/smb-resources/RemoveUser")
    Call<APIResponse> removePreviousUser(@Body JustPayTransaction transaction) throws Exception;


    /**
     * send user login details
     *
     * @param userLogin user login details
     */
    @POST("api/login")
    Call<Login> userLogin(@Body Login userLogin) throws Exception;

    /**
     * register a pin
     *
     * @param pin new pin details
     */
    @POST("api/register_pin")
    Call<CommonResponse> registerPIN(@Header("authorization") String authorization, @Body PIN pin) throws Exception;

    /**
     * get bank and branch details
     */
    @GET("api/banks")
    Call<ArrayList<Bank>> getBankAndBranches() throws Exception;

    /**
     * get user bank and branch details
     *
     * @param user_data current user requested transaction request body
     */
    @GET("api/bank_info")
    Call<UserBankAccount> getUserBankDetails(@Header("authorization") String authorization, @QueryMap Map<String, Object> user_data) throws Exception;

    /**
     * register user bank account details
     *
     * @param userBankDetails user bank account details
     */
    @POST("api/bank_info")
    Call<UserBankAccount> registerUserBankDetails(@Header("authorization") String authorization, @Body UserBankAccount userBankDetails) throws Exception;

    /**
     * logout current user
     *
     * @param logout current logged in user details
     */
    @POST("api/logout")
    Call<Login> logout(@Body Login logout) throws Exception;

    /**
     * update current user PIN
     *
     * @param pin updated user PIN details
     */
    @POST("api/update_pin")
    Call<Login> updatePIN(@Header("authorization") String authorization, @Body PIN pin) throws Exception;

    /**
     * validate current user PIN
     *
     * @param pin current user PIN details
     */
    @POST("api/validate_pin")
    Call<PIN> validatePIN(@Header("authorization") String authorization, @Body PIN pin) throws Exception;

    /**
     * get all pending transactions
     *
     * @param transaction_req current user requested transaction request body
     */
    @GET("api/pending_transactions")
    Call<PendingTransactions> getPendingTransactions(@Header("authorization") String authorization, @QueryMap Map<String, Object> transaction_req) throws Exception;

    /**
     * get selected pending transaction
     *
     * @param id selected transaction id
     */
    @GET("api/pending_transaction/{id}/show")
    Call<PendingTransactions> getSelectedPendingTransactions(@Header("authorization") String authorization, @Path("id") int id) throws Exception;

    /**
     * accept selected transaction
     *
     * @param id          selected transaction id
     * @param transaction selected transaction data
     */
    @POST("api/pending_transaction/{id}/accept")
    Call<PendingTransactions> acceptSelectedTransaction(@Header("authorization") String authorization, @Path("id") int id, @Body PendingTransactions transaction) throws Exception;

    /**
     * reject selected transaction
     *
     * @param id          selected transaction id
     * @param transaction selected transaction data
     */
    @POST("api/pending_transaction/{id}/decline")
    Call<PendingTransactions> rejectSelectedTransaction(@Header("authorization") String authorization, @Path("id") int id, @Body PendingTransactions transaction) throws Exception;

    /**
     * request customer details via customer NIC
     *
     * @param value    selected user's NIC no
     * @param customer current searching customer data
     */
    @GET("api/request_payment/nic_lookup/{value}")
    Call<Customer> requestCustomPaymentByNIC(@Header("authorization") String authorization, @Path("value") String value, @QueryMap Map<String, Object> customer) throws Exception;

    /**
     * request customer payment proceed
     *
     * @param transaction current processing transaction details
     */
    @POST("api/request_payment/payment_confirm")
    Call<CommonResponse> proceedCustomerPayment(@Header("authorization") String authorization, @Body Transaction transaction) throws Exception;

    /**
     * search among available customers
     *
     * @param search_query current user searching query
     * @param customer_req current user requested customers details body
     */
    @GET("api/request_payment/customer_lookup/{value}")
    Call<Customer> searchAmongAvailableCustomers(@Header("authorization") String authorization, @Path("value") String search_query, @QueryMap Map<String, Object> customer_req) throws Exception;

    /**
     * get all available customers
     *
     * @param customer_req current user requested customers details body
     */
    @GET("api/my_customers")
    Call<Customer> getAllAvailableCustomers(@Header("authorization") String authorization, @QueryMap Map<String, Object> customer_req) throws Exception;

    /**
     * get selected customer's details
     *
     * @param customer_id  selected customer's id
     * @param customer_req current user requested customers details body
     */
    @GET("api/my_customers/{id}/show")
    Call<Customer> getSelectedCustomerDetails(@Header("authorization") String authorization, @Path("id") int customer_id, @QueryMap Map<String, Object> customer_req) throws Exception;

    /**
     * get current merchant data
     *
     * @param merchant_id  logged in merchant's id
     * @param merchant_req current user requested body
     */
    @GET("api/merchant/{merchant_id}/show")
    Call<Merchant> getCurrentMerchantDetails(@Header("authorization") String authorization, @Path("merchant_id") String merchant_id, @QueryMap Map<String, Object> merchant_req) throws Exception;

    /**
     * get all my transactions
     *
     * @param customer_req current user requested transactions details body
     */
    @GET("api/my_transactions")
    Call<Transaction> getAllMyTransactions(@Header("authorization") String authorization, @QueryMap Map<String, Object> customer_req) throws Exception;

}
