package com.redapplemart.rukula.interfaces;


import retrofit2.Call;

/**
 * Created by SYSDEK Pvt Ltd on 7/31/17.
 */

public interface RetrofitResponseInterface<T> {

    /**
     * this function fired when http request contains a valid response
     *
     * @param call       current http call with retrofit
     * @param response   valid response data
     * @param request_id user provided custom request id to identify the executed request
     **/
    void onRetrofitSuccess(Call<T> call, T response, int request_id);

    /**
     * this function fired when http request failed
     *
     * @param call current http call with retrofit
     **/
    void onRetrofitFailed(Call<T> call);

}
