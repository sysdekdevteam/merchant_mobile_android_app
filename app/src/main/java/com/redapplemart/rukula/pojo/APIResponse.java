package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class APIResponse implements Parcelable {

    private String TimeStamp;
    private String ReferenceId;
    private String ResponseCode;
    private String ResponseDesc;
    private String Challenge;
    private String LCComment;

    private String CustomerToken;
    private String AccountReference;

    private String TranId;

    public APIResponse() {
    }

    protected APIResponse(Parcel in) {
        TimeStamp = in.readString();
        ReferenceId = in.readString();
        ResponseCode = in.readString();
        ResponseDesc = in.readString();
        Challenge = in.readString();
        LCComment = in.readString();
        CustomerToken = in.readString();
        AccountReference = in.readString();
        TranId = in.readString();
    }

    public static final Creator<APIResponse> CREATOR = new Creator<APIResponse>() {
        @Override
        public APIResponse createFromParcel(Parcel in) {
            return new APIResponse(in);
        }

        @Override
        public APIResponse[] newArray(int size) {
            return new APIResponse[size];
        }
    };

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseDesc() {
        return ResponseDesc;
    }

    public void setResponseDesc(String responseDesc) {
        ResponseDesc = responseDesc;
    }

    public String getChallenge() {
        return Challenge;
    }

    public void setChallenge(String challenge) {
        Challenge = challenge;
    }

    public String getLCComment() {
        return LCComment;
    }

    public void setLCComment(String LCComment) {
        this.LCComment = LCComment;
    }


    public String getCustomerToken() {
        return CustomerToken;
    }

    public void setCustomerToken(String customerToken) {
        CustomerToken = customerToken;
    }

    public String getAccountReference() {
        return AccountReference;
    }

    public void setAccountReference(String accountReference) {
        AccountReference = accountReference;
    }


    public String getTranId() {
        return TranId;
    }

    public void setTranId(String tranId) {
        TranId = tranId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TimeStamp);
        dest.writeString(ReferenceId);
        dest.writeString(ResponseCode);
        dest.writeString(ResponseDesc);
        dest.writeString(Challenge);
        dest.writeString(LCComment);
        dest.writeString(CustomerToken);
        dest.writeString(AccountReference);
        dest.writeString(TranId);
    }
}
