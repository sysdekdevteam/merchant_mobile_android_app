package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Bank implements Parcelable {

    private String bank;
    private String bank_code;
    private ArrayList<Bank> branches;

    private String branch_code;
    private String branch_name;

    public Bank() {
    }

    protected Bank(Parcel in) {
        bank = in.readString();
        bank_code = in.readString();
        branches = in.createTypedArrayList(Bank.CREATOR);
        branch_code = in.readString();
        branch_name = in.readString();
    }

    public static final Creator<Bank> CREATOR = new Creator<Bank>() {
        @Override
        public Bank createFromParcel(Parcel in) {
            return new Bank(in);
        }

        @Override
        public Bank[] newArray(int size) {
            return new Bank[size];
        }
    };

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public ArrayList<Bank> getBranches() {
        return branches;
    }

    public void setBranches(ArrayList<Bank> branches) {
        this.branches = branches;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bank);
        dest.writeString(bank_code);
        dest.writeTypedList(branches);
        dest.writeString(branch_code);
        dest.writeString(branch_name);
    }
}
