package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class CommonResponse implements Parcelable {

    private String status;

    public CommonResponse() {
    }

    protected CommonResponse(Parcel in) {
        status = in.readString();
    }

    public static final Creator<CommonResponse> CREATOR = new Creator<CommonResponse>() {
        @Override
        public CommonResponse createFromParcel(Parcel in) {
            return new CommonResponse(in);
        }

        @Override
        public CommonResponse[] newArray(int size) {
            return new CommonResponse[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
    }
}
