package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Customer implements Parcelable {

    private int id;

    private String first_name;
    private String last_name;
    private String nic;
    private int my_customer;//if value is 0-not this merchant's customer, if value is 1-merchant's customer

    private String installment_value;
    private String outstanding_as_of_today;
    private String total_outstanding;

    private String device_id;
    private String merchant_id;

    //my customer search results data
    private Customer _meta;
    private int page;
    private int per_page;
    private int total_items;
    private int total_pages;

    private ArrayList<Customer> items;
    private String last_date_of_purchase;
    private int member_id;

    //customer applications data
    private String address_1;
    private String address_2;
    private String address_3;
    private ArrayList<CustomerApplications> applications;
    private String city;
    private String electricity_account_no;
    private String electricity_bill_amount;
    private String last_payment_completed_date;
    private String phone_no;

    private String product_name;

    private String status;
    private String payment_due;
    private String application_id;

    public Customer() {
    }

    protected Customer(Parcel in) {
        id = in.readInt();
        first_name = in.readString();
        last_name = in.readString();
        nic = in.readString();
        my_customer = in.readInt();
        installment_value = in.readString();
        outstanding_as_of_today = in.readString();
        total_outstanding = in.readString();
        device_id = in.readString();
        merchant_id = in.readString();
        _meta = in.readParcelable(Customer.class.getClassLoader());
        page = in.readInt();
        per_page = in.readInt();
        total_items = in.readInt();
        total_pages = in.readInt();
        items = in.createTypedArrayList(Customer.CREATOR);
        last_date_of_purchase = in.readString();
        member_id = in.readInt();
        address_1 = in.readString();
        address_2 = in.readString();
        address_3 = in.readString();
        applications = in.createTypedArrayList(CustomerApplications.CREATOR);
        city = in.readString();
        electricity_account_no = in.readString();
        electricity_bill_amount = in.readString();
        last_payment_completed_date = in.readString();
        phone_no = in.readString();
        product_name = in.readString();
        status = in.readString();
        payment_due = in.readString();
        application_id = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public int getMy_customer() {
        return my_customer;
    }

    public void setMy_customer(int my_customer) {
        this.my_customer = my_customer;
    }


    public String getInstallment_value() {
        return installment_value;
    }

    public void setInstallment_value(String installment_value) {
        this.installment_value = installment_value;
    }

    public String getOutstanding_as_of_today() {
        return outstanding_as_of_today;
    }

    public void setOutstanding_as_of_today(String outstanding_as_of_today) {
        this.outstanding_as_of_today = outstanding_as_of_today;
    }

    public String getTotal_outstanding() {
        return total_outstanding;
    }

    public void setTotal_outstanding(String total_outstanding) {
        this.total_outstanding = total_outstanding;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public Customer get_meta() {
        return _meta;
    }

    public void set_meta(Customer _meta) {
        this._meta = _meta;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_items() {
        return total_items;
    }

    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<Customer> getItems() {
        return items;
    }

    public void setItems(ArrayList<Customer> items) {
        this.items = items;
    }

    public String getLast_date_of_purchase() {
        return last_date_of_purchase;
    }

    public void setLast_date_of_purchase(String last_date_of_purchase) {
        this.last_date_of_purchase = last_date_of_purchase;
    }

    public int getMember_id() {
        return member_id;
    }

    public void setMember_id(int member_id) {
        this.member_id = member_id;
    }


    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getAddress_3() {
        return address_3;
    }

    public void setAddress_3(String address_3) {
        this.address_3 = address_3;
    }

    public ArrayList<CustomerApplications> getApplications() {
        return applications;
    }

    public void setApplications(ArrayList<CustomerApplications> applications) {
        this.applications = applications;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getElectricity_account_no() {
        return electricity_account_no;
    }

    public void setElectricity_account_no(String electricity_account_no) {
        this.electricity_account_no = electricity_account_no;
    }

    public String getElectricity_bill_amount() {
        return electricity_bill_amount;
    }

    public void setElectricity_bill_amount(String electricity_bill_amount) {
        this.electricity_bill_amount = electricity_bill_amount;
    }

    public String getLast_payment_completed_date() {
        return last_payment_completed_date;
    }

    public void setLast_payment_completed_date(String last_payment_completed_date) {
        this.last_payment_completed_date = last_payment_completed_date;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }


    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayment_due() {
        return payment_due;
    }

    public void setPayment_due(String payment_due) {
        this.payment_due = payment_due;
    }


    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    /***get calculated installment amount**/
    public double getCalculatedInstallment() {
        if (this.getApplications() == null || this.getApplications().size() == 0) return 0;

        double value = 0;
        for (CustomerApplications application : this.getApplications()) {
            value += application.getInstallment_value();
        }

        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(first_name);
        parcel.writeString(last_name);
        parcel.writeString(nic);
        parcel.writeInt(my_customer);
        parcel.writeString(installment_value);
        parcel.writeString(outstanding_as_of_today);
        parcel.writeString(total_outstanding);
        parcel.writeString(device_id);
        parcel.writeString(merchant_id);
        parcel.writeParcelable(_meta, i);
        parcel.writeInt(page);
        parcel.writeInt(per_page);
        parcel.writeInt(total_items);
        parcel.writeInt(total_pages);
        parcel.writeTypedList(items);
        parcel.writeString(last_date_of_purchase);
        parcel.writeInt(member_id);
        parcel.writeString(address_1);
        parcel.writeString(address_2);
        parcel.writeString(address_3);
        parcel.writeTypedList(applications);
        parcel.writeString(city);
        parcel.writeString(electricity_account_no);
        parcel.writeString(electricity_bill_amount);
        parcel.writeString(last_payment_completed_date);
        parcel.writeString(phone_no);
        parcel.writeString(product_name);
        parcel.writeString(status);
        parcel.writeString(payment_due);
        parcel.writeString(application_id);
    }
}
