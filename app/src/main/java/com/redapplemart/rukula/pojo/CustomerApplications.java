package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class CustomerApplications implements Parcelable {

    private int application_id;
    private String date_of_purchase;
    private int deposit_value;
    private int id;
    private int installment_value;
    private String product_name;
    private String status;
    private int total_value;

    private ArrayList<CustomerApplications> payment_due_dates;
    private int amount_paid;
    private int balance;
    private String due_date;
    private String payment_completion_date;

    public CustomerApplications() {
    }

    protected CustomerApplications(Parcel in) {
        application_id = in.readInt();
        date_of_purchase = in.readString();
        deposit_value = in.readInt();
        id = in.readInt();
        installment_value = in.readInt();
        product_name = in.readString();
        status = in.readString();
        total_value = in.readInt();
        payment_due_dates = in.createTypedArrayList(CustomerApplications.CREATOR);
        amount_paid = in.readInt();
        balance = in.readInt();
        due_date = in.readString();
        payment_completion_date = in.readString();
    }

    public static final Creator<CustomerApplications> CREATOR = new Creator<CustomerApplications>() {
        @Override
        public CustomerApplications createFromParcel(Parcel in) {
            return new CustomerApplications(in);
        }

        @Override
        public CustomerApplications[] newArray(int size) {
            return new CustomerApplications[size];
        }
    };

    public int getApplication_id() {
        return application_id;
    }

    public void setApplication_id(int application_id) {
        this.application_id = application_id;
    }

    public String getDate_of_purchase() {
        return date_of_purchase;
    }

    public void setDate_of_purchase(String date_of_purchase) {
        this.date_of_purchase = date_of_purchase;
    }

    public int getDeposit_value() {
        return deposit_value;
    }

    public void setDeposit_value(int deposit_value) {
        this.deposit_value = deposit_value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getInstallment_value() {
        return installment_value;
    }

    public void setInstallment_value(int installment_value) {
        this.installment_value = installment_value;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTotal_value() {
        return total_value;
    }

    public void setTotal_value(int total_value) {
        this.total_value = total_value;
    }

    public ArrayList<CustomerApplications> getPayment_due_dates() {
        return payment_due_dates;
    }

    public void setPayment_due_dates(ArrayList<CustomerApplications> payment_due_dates) {
        this.payment_due_dates = payment_due_dates;
    }

    public int getAmount_paid() {
        return amount_paid;
    }

    public void setAmount_paid(int amount_paid) {
        this.amount_paid = amount_paid;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getPayment_completion_date() {
        return payment_completion_date;
    }

    public void setPayment_completion_date(String payment_completion_date) {
        this.payment_completion_date = payment_completion_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(application_id);
        dest.writeString(date_of_purchase);
        dest.writeInt(deposit_value);
        dest.writeInt(id);
        dest.writeInt(installment_value);
        dest.writeString(product_name);
        dest.writeString(status);
        dest.writeInt(total_value);
        dest.writeTypedList(payment_due_dates);
        dest.writeInt(amount_paid);
        dest.writeInt(balance);
        dest.writeString(due_date);
        dest.writeString(payment_completion_date);
    }
}
