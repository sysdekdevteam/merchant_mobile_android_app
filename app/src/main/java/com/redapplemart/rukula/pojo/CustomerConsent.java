package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomerConsent implements Parcelable {

    private String MerchantID;
    private String MerchantToken;
    private String ReferenceId;
    private String CustomerNic;
    private String TimeStamp;
    private String CustConsent;

    public CustomerConsent() {
    }

    protected CustomerConsent(Parcel in) {
        MerchantID = in.readString();
        MerchantToken = in.readString();
        ReferenceId = in.readString();
        CustomerNic = in.readString();
        TimeStamp = in.readString();
        CustConsent = in.readString();
    }

    public static final Creator<CustomerConsent> CREATOR = new Creator<CustomerConsent>() {
        @Override
        public CustomerConsent createFromParcel(Parcel in) {
            return new CustomerConsent(in);
        }

        @Override
        public CustomerConsent[] newArray(int size) {
            return new CustomerConsent[size];
        }
    };

    public String getMerchantID() {
        return MerchantID;
    }

    public void setMerchantID(String merchantID) {
        MerchantID = merchantID;
    }

    public String getMerchantToken() {
        return MerchantToken;
    }

    public void setMerchantToken(String merchantToken) {
        MerchantToken = merchantToken;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getCustomerNic() {
        return CustomerNic;
    }

    public void setCustomerNic(String customerNic) {
        CustomerNic = customerNic;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getCustConsent() {
        return CustConsent;
    }

    public void setCustConsent(String custConsent) {
        CustConsent = custConsent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MerchantID);
        dest.writeString(MerchantToken);
        dest.writeString(ReferenceId);
        dest.writeString(CustomerNic);
        dest.writeString(TimeStamp);
        dest.writeString(CustConsent);
    }
}
