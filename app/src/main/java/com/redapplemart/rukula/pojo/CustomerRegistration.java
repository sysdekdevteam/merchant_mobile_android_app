package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class CustomerRegistration implements Parcelable {

    private String MerchantID;
    private String MerchantToken;
    private String ReferenceId;
    private String TimeStamp;
    private String CustomerName;
    private String CustomerNic;
    private String MobileNumber;
    private String CustomerEmail;
    private String TypeCode;
    private String BankCode;
    private String BranchCode;
    private String AccountNumber;
    private String JustPayCode;
    private String DeviceID;
    private String Platform;

    public CustomerRegistration() {
    }

    protected CustomerRegistration(Parcel in) {
        MerchantID = in.readString();
        MerchantToken = in.readString();
        ReferenceId = in.readString();
        TimeStamp = in.readString();
        CustomerName = in.readString();
        CustomerNic = in.readString();
        MobileNumber = in.readString();
        CustomerEmail = in.readString();
        TypeCode = in.readString();
        BankCode = in.readString();
        BranchCode = in.readString();
        AccountNumber = in.readString();
        JustPayCode = in.readString();
        DeviceID = in.readString();
        Platform = in.readString();
    }

    public static final Creator<CustomerRegistration> CREATOR = new Creator<CustomerRegistration>() {
        @Override
        public CustomerRegistration createFromParcel(Parcel in) {
            return new CustomerRegistration(in);
        }

        @Override
        public CustomerRegistration[] newArray(int size) {
            return new CustomerRegistration[size];
        }
    };

    public String getMerchantID() {
        return MerchantID;
    }

    public void setMerchantID(String merchantID) {
        MerchantID = merchantID;
    }

    public String getMerchantToken() {
        return MerchantToken;
    }

    public void setMerchantToken(String merchantToken) {
        MerchantToken = merchantToken;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getCustomerNic() {
        return CustomerNic;
    }

    public void setCustomerNic(String customerNic) {
        CustomerNic = customerNic;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getCustomerEmail() {
        return CustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        CustomerEmail = customerEmail;
    }

    public String getTypeCode() {
        return TypeCode;
    }

    public void setTypeCode(String typeCode) {
        TypeCode = typeCode;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }

    public String getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(String branchCode) {
        BranchCode = branchCode;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getJustPayCode() {
        return JustPayCode;
    }

    public void setJustPayCode(String justPayCode) {
        JustPayCode = justPayCode;
    }

    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String deviceID) {
        DeviceID = deviceID;
    }

    public String getPlatform() {
        return Platform;
    }

    public void setPlatform(String platform) {
        Platform = platform;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MerchantID);
        dest.writeString(MerchantToken);
        dest.writeString(ReferenceId);
        dest.writeString(TimeStamp);
        dest.writeString(CustomerName);
        dest.writeString(CustomerNic);
        dest.writeString(MobileNumber);
        dest.writeString(CustomerEmail);
        dest.writeString(TypeCode);
        dest.writeString(BankCode);
        dest.writeString(BranchCode);
        dest.writeString(AccountNumber);
        dest.writeString(JustPayCode);
        dest.writeString(DeviceID);
        dest.writeString(Platform);
    }
}
