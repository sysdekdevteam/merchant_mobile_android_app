package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class HTTPResponse implements Parcelable {

    private String response;
    private int response_code;

    public HTTPResponse() {
    }

    protected HTTPResponse(Parcel in) {
        response = in.readString();
        response_code = in.readInt();
    }

    public static final Creator<HTTPResponse> CREATOR = new Creator<HTTPResponse>() {
        @Override
        public HTTPResponse createFromParcel(Parcel in) {
            return new HTTPResponse(in);
        }

        @Override
        public HTTPResponse[] newArray(int size) {
            return new HTTPResponse[size];
        }
    };

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResponse_code() {
        return response_code;
    }

    public void setResponse_code(int response_code) {
        this.response_code = response_code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(response);
        dest.writeInt(response_code);
    }
}
