package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class JustPayTransaction implements Parcelable {

    private String MerchantID;
    private String MerchantToken;
    private String ReferenceId;
    private String TimeStamp;
    private String SubMerchantID;
    private String CustomerToken;
    private String AccountRef;
    private String TxnAmount;

    public JustPayTransaction() {
    }

    protected JustPayTransaction(Parcel in) {
        MerchantID = in.readString();
        MerchantToken = in.readString();
        ReferenceId = in.readString();
        TimeStamp = in.readString();
        SubMerchantID = in.readString();
        CustomerToken = in.readString();
        AccountRef = in.readString();
        TxnAmount = in.readString();
    }

    public static final Creator<JustPayTransaction> CREATOR = new Creator<JustPayTransaction>() {
        @Override
        public JustPayTransaction createFromParcel(Parcel in) {
            return new JustPayTransaction(in);
        }

        @Override
        public JustPayTransaction[] newArray(int size) {
            return new JustPayTransaction[size];
        }
    };

    public String getMerchantID() {
        return MerchantID;
    }

    public void setMerchantID(String merchantID) {
        MerchantID = merchantID;
    }

    public String getMerchantToken() {
        return MerchantToken;
    }

    public void setMerchantToken(String merchantToken) {
        MerchantToken = merchantToken;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public void setReferenceId(String referenceId) {
        ReferenceId = referenceId;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getSubMerchantID() {
        return SubMerchantID;
    }

    public void setSubMerchantID(String subMerchantID) {
        SubMerchantID = subMerchantID;
    }

    public String getCustomerToken() {
        return CustomerToken;
    }

    public void setCustomerToken(String customerToken) {
        CustomerToken = customerToken;
    }

    public String getAccountRef() {
        return AccountRef;
    }

    public void setAccountRef(String accountRef) {
        AccountRef = accountRef;
    }

    public String getTxnAmount() {
        return TxnAmount;
    }

    public void setTxnAmount(String txnAmount) {
        TxnAmount = txnAmount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(MerchantID);
        dest.writeString(MerchantToken);
        dest.writeString(ReferenceId);
        dest.writeString(TimeStamp);
        dest.writeString(SubMerchantID);
        dest.writeString(CustomerToken);
        dest.writeString(AccountRef);
        dest.writeString(TxnAmount);
    }
}
