package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Login implements Parcelable {

    private String merchant_id;
    private String password;
    private String device_id;

    //response
    private int status;
    private String token;

    public Login() {
    }

    protected Login(Parcel in) {
        merchant_id = in.readString();
        password = in.readString();
        device_id = in.readString();
        status = in.readInt();
        token = in.readString();
    }

    public static final Creator<Login> CREATOR = new Creator<Login>() {
        @Override
        public Login createFromParcel(Parcel in) {
            return new Login(in);
        }

        @Override
        public Login[] newArray(int size) {
            return new Login[size];
        }
    };

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(merchant_id);
        dest.writeString(password);
        dest.writeString(device_id);
        dest.writeInt(status);
        dest.writeString(token);
    }
}
