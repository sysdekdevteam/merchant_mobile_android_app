package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Merchant implements Parcelable {

    private String address;
    private String email;
    private String full_name;
    private String mobile_no;
    private String nic;

    public Merchant() {
    }

    protected Merchant(Parcel in) {
        address = in.readString();
        email = in.readString();
        full_name = in.readString();
        mobile_no = in.readString();
        nic = in.readString();
    }

    public static final Creator<Merchant> CREATOR = new Creator<Merchant>() {
        @Override
        public Merchant createFromParcel(Parcel in) {
            return new Merchant(in);
        }

        @Override
        public Merchant[] newArray(int size) {
            return new Merchant[size];
        }
    };

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(full_name);
        dest.writeString(mobile_no);
        dest.writeString(nic);
    }
}
