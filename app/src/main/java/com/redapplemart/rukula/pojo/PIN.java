package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class PIN implements Parcelable {

    private String merchant_id;
    private String device_id;
    private String pin;
    private String token;

    //pin update
    private String old_pin;
    private String new_pin;

    //response
    private String message;
    private int status;

    public PIN() {
    }

    protected PIN(Parcel in) {
        merchant_id = in.readString();
        device_id = in.readString();
        pin = in.readString();
        token = in.readString();
        old_pin = in.readString();
        new_pin = in.readString();
        message = in.readString();
        status = in.readInt();
    }

    public static final Creator<PIN> CREATOR = new Creator<PIN>() {
        @Override
        public PIN createFromParcel(Parcel in) {
            return new PIN(in);
        }

        @Override
        public PIN[] newArray(int size) {
            return new PIN[size];
        }
    };

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOld_pin() {
        return old_pin;
    }

    public void setOld_pin(String old_pin) {
        this.old_pin = old_pin;
    }

    public String getNew_pin() {
        return new_pin;
    }

    public void setNew_pin(String new_pin) {
        this.new_pin = new_pin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(merchant_id);
        dest.writeString(device_id);
        dest.writeString(pin);
        dest.writeString(token);
        dest.writeString(old_pin);
        dest.writeString(new_pin);
        dest.writeString(message);
        dest.writeInt(status);
    }
}
