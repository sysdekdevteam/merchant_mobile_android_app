package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class PendingTransactions implements Parcelable {

    private String device_id;
    private String merchant_id;
    private String token;
    private int page;
    private int per_page;

    //response
    private String status;
    //page details
    private PendingTransactions meta;
    private int total_items;
    private int total_pages;
    //items details
    private ArrayList<PendingTransactions> items;
    private String amount;
    private String date_and_time;
    private int id;
    private String name;
    private String nic;
    private int transaction_id;

    public PendingTransactions() {
    }

    protected PendingTransactions(Parcel in) {
        device_id = in.readString();
        merchant_id = in.readString();
        token = in.readString();
        page = in.readInt();
        per_page = in.readInt();
        status = in.readString();
        meta = in.readParcelable(PendingTransactions.class.getClassLoader());
        total_items = in.readInt();
        total_pages = in.readInt();
        items = in.createTypedArrayList(PendingTransactions.CREATOR);
        amount = in.readString();
        date_and_time = in.readString();
        id = in.readInt();
        name = in.readString();
        nic = in.readString();
        transaction_id = in.readInt();
    }

    public static final Creator<PendingTransactions> CREATOR = new Creator<PendingTransactions>() {
        @Override
        public PendingTransactions createFromParcel(Parcel in) {
            return new PendingTransactions(in);
        }

        @Override
        public PendingTransactions[] newArray(int size) {
            return new PendingTransactions[size];
        }
    };

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public PendingTransactions getMeta() {
        return meta;
    }

    public void setMeta(PendingTransactions meta) {
        this.meta = meta;
    }

    public int getTotal_items() {
        return total_items;
    }

    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<PendingTransactions> getItems() {
        return items;
    }

    public void setItems(ArrayList<PendingTransactions> items) {
        this.items = items;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate_and_time() {
        return date_and_time;
    }

    public void setDate_and_time(String date_and_time) {
        this.date_and_time = date_and_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(device_id);
        dest.writeString(merchant_id);
        dest.writeString(token);
        dest.writeInt(page);
        dest.writeInt(per_page);
        dest.writeString(status);
        dest.writeParcelable(meta, flags);
        dest.writeInt(total_items);
        dest.writeInt(total_pages);
        dest.writeTypedList(items);
        dest.writeString(amount);
        dest.writeString(date_and_time);
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(nic);
        dest.writeInt(transaction_id);
    }
}
