package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Transaction implements Parcelable {

    private String payment_value;
    private String nic;
    private String merchant_id;
    private String device_id;
    private String reference_id;
    private String time_stamp;
    private String tran_id;

    //my transactions data
    private Transaction _meta;
    private int page;
    private int per_page;
    private int total_items;
    private int total_pages;

    private ArrayList<Transaction> items;
    private String amount;
    private String date_and_time;
    private String initiator;
    private String method;
    private String status;
    private int transaction_id;

    private String customer_name;

    public Transaction() {
    }

    protected Transaction(Parcel in) {
        payment_value = in.readString();
        nic = in.readString();
        merchant_id = in.readString();
        device_id = in.readString();
        reference_id = in.readString();
        tran_id = in.readString();
        time_stamp = in.readString();
        _meta = in.readParcelable(Transaction.class.getClassLoader());
        page = in.readInt();
        per_page = in.readInt();
        total_items = in.readInt();
        total_pages = in.readInt();
        items = in.createTypedArrayList(Transaction.CREATOR);
        amount = in.readString();
        date_and_time = in.readString();
        initiator = in.readString();
        method = in.readString();
        status = in.readString();
        transaction_id = in.readInt();
        customer_name = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public String getPayment_value() {
        return payment_value;
    }

    public void setPayment_value(String payment_value) {
        this.payment_value = payment_value;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getTime_stamp() {
        return time_stamp;
    }

    public void setTime_stamp(String time_stamp) {
        this.time_stamp = time_stamp;
    }


    public Transaction get_meta() {
        return _meta;
    }

    public void set_meta(Transaction _meta) {
        this._meta = _meta;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_items() {
        return total_items;
    }

    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<Transaction> getItems() {
        return items;
    }

    public void setItems(ArrayList<Transaction> items) {
        this.items = items;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate_and_time() {
        return date_and_time;
    }

    public void setDate_and_time(String date_and_time) {
        this.date_and_time = date_and_time;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(int transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(payment_value);
        dest.writeString(nic);
        dest.writeString(merchant_id);
        dest.writeString(device_id);
        dest.writeString(reference_id);
        dest.writeString(tran_id);
        dest.writeString(time_stamp);
        dest.writeParcelable(_meta, flags);
        dest.writeInt(page);
        dest.writeInt(per_page);
        dest.writeInt(total_items);
        dest.writeInt(total_pages);
        dest.writeTypedList(items);
        dest.writeString(amount);
        dest.writeString(date_and_time);
        dest.writeString(initiator);
        dest.writeString(method);
        dest.writeString(status);
        dest.writeInt(transaction_id);
        dest.writeString(customer_name);
    }
}
