package com.redapplemart.rukula.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class UserBankAccount implements Parcelable {

    //get
    private String merchant_id;
    private String device_id;
    private String token;

    //post extra data
    private String bank_code;
    private String branch_code;
    private String account_last_four_digits;
    private String reference_id;
    private String token_id;
    private String account_reference;
    private String customer_token;

    //response
    //get
    private String bank;
    private String branch_name;
    //post
    private String status;

    public UserBankAccount() {
    }

    protected UserBankAccount(Parcel in) {
        merchant_id = in.readString();
        device_id = in.readString();
        token = in.readString();
        bank_code = in.readString();
        branch_code = in.readString();
        account_last_four_digits = in.readString();
        reference_id = in.readString();
        token_id = in.readString();
        account_reference = in.readString();
        customer_token = in.readString();
        bank = in.readString();
        branch_name = in.readString();
        status = in.readString();
    }

    public static final Creator<UserBankAccount> CREATOR = new Creator<UserBankAccount>() {
        @Override
        public UserBankAccount createFromParcel(Parcel in) {
            return new UserBankAccount(in);
        }

        @Override
        public UserBankAccount[] newArray(int size) {
            return new UserBankAccount[size];
        }
    };

    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getAccount_last_four_digits() {
        return account_last_four_digits;
    }

    public void setAccount_last_four_digits(String account_last_four_digits) {
        this.account_last_four_digits = account_last_four_digits;
    }

    public String getReference_id() {
        return reference_id;
    }

    public void setReference_id(String reference_id) {
        this.reference_id = reference_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomer_token() {
        return customer_token;
    }

    public void setCustomer_token(String customer_token) {
        this.customer_token = customer_token;
    }

    public String getAccount_reference() {
        return account_reference;
    }

    public void setAccount_reference(String account_reference) {
        this.account_reference = account_reference;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(merchant_id);
        dest.writeString(device_id);
        dest.writeString(token);
        dest.writeString(bank_code);
        dest.writeString(branch_code);
        dest.writeString(account_last_four_digits);
        dest.writeString(reference_id);
        dest.writeString(token_id);
        dest.writeString(account_reference);
        dest.writeString(customer_token);
        dest.writeString(bank);
        dest.writeString(branch_name);
        dest.writeString(status);
    }
}
