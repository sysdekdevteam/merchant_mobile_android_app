package com.redapplemart.rukula.popup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.consts.CustomerLookupSort;

public class CustomerSortPopup extends Dialog implements View.OnClickListener {

    private RadioGroup mRadioBtnGroup;

    private CustomerSortPopupInterface mActionListener;

    public CustomerSortPopup(@NonNull Context context) {
        super(context);
    }

    public CustomerSortPopup(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected CustomerSortPopup(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_customer_sort);

        TextView cancel = findViewById(R.id.custom_sort_dialog_close_txtview);
        TextView ok = findViewById(R.id.custom_sort_dialog_ok_txtview);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

        mRadioBtnGroup = findViewById(R.id.custom_sort_dialog_radio_group);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.custom_sort_dialog_close_txtview:
                dismiss();
                break;

            case R.id.custom_sort_dialog_ok_txtview:

                int radio_id = mRadioBtnGroup.getCheckedRadioButtonId();
                CustomerLookupSort sort_order = CustomerLookupSort.SORT_NONE;
                switch (radio_id) {
                    case R.id.custom_sort_dialog_radio_1:
                        sort_order = CustomerLookupSort.A_TO_Z_NAME_ORDER;
                        break;
                    case R.id.custom_sort_dialog_radio_2:
                        sort_order = CustomerLookupSort.Z_TO_A_NAME_ORDER;
                        break;
                    case R.id.custom_sort_dialog_radio_3:
                        sort_order = CustomerLookupSort.INSTALLMENT_AMOUNTS;
                        break;
                    case R.id.custom_sort_dialog_radio_4:
                        sort_order = CustomerLookupSort.LATEST_APPLICATION;
                        break;
                }

                if (mActionListener != null) mActionListener.onSortSelected(sort_order);
                dismiss();
                break;
        }
    }

    /**
     * set popup action listener
     ***/
    public void setPopupActionListener(CustomerSortPopupInterface listener) {
        mActionListener = listener;
    }


    /**
     * customer sorting popup action listener interface
     ***/
    public interface CustomerSortPopupInterface {
        void onSortSelected(CustomerLookupSort sorting);
    }

}
