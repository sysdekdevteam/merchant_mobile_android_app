package com.redapplemart.rukula.popup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.consts.ApplicationsLookupSort;

public class MyApplicationsSortPopup extends Dialog implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private MyApplicationsSortPopup.ApplicationSortPopupInterface mActionListener;
    private ApplicationsLookupSort mApplicationSortOrder = ApplicationsLookupSort.SORT_NONE;

    private RadioGroup mRadioGroup1, mRadioGroup2, mRadioGroup3, mRadioGroup4;

    public MyApplicationsSortPopup(@NonNull Context context) {
        super(context);
    }

    public MyApplicationsSortPopup(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected MyApplicationsSortPopup(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_my_applications_sort);

        TextView cancel = findViewById(R.id.custom_application_sort_dialog_close_txtview);
        TextView ok = findViewById(R.id.custom_application_sort_dialog_ok_txtview);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

        mRadioGroup1 = findViewById(R.id.custom_my_app_sort_dialog_app_id_radio_group);
        mRadioGroup2 = findViewById(R.id.custom_my_app_sort_dialog_app_stat_radio_group);
        mRadioGroup3 = findViewById(R.id.custom_my_app_sort_dialog_installment_radio_group);
        mRadioGroup4 = findViewById(R.id.custom_my_app_sort_dialog_app_radio_group);
        mRadioGroup1.setOnCheckedChangeListener(this);
        mRadioGroup2.setOnCheckedChangeListener(this);
        mRadioGroup3.setOnCheckedChangeListener(this);
        mRadioGroup4.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.custom_application_sort_dialog_close_txtview:
                dismiss();
                break;

            case R.id.custom_application_sort_dialog_ok_txtview:

                if (mActionListener != null)
                    mActionListener.onSortSelected(mApplicationSortOrder);
                dismiss();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {

            case R.id.custom_my_app_sort_dialog_app_id_radio_group:
                if (mRadioGroup2.getCheckedRadioButtonId() != -1) mRadioGroup2.check(-1);
                if (mRadioGroup3.getCheckedRadioButtonId() != -1) mRadioGroup3.check(-1);
                if (mRadioGroup4.getCheckedRadioButtonId() != -1) mRadioGroup4.check(-1);

                if (checkedId == R.id.custom_my_app_sort_dialog_app_id_radio_1)
                    mApplicationSortOrder = ApplicationsLookupSort.APPLICATION_ID_ASC;
                else
                    mApplicationSortOrder = ApplicationsLookupSort.APPLICATION_ID_DESC;
                break;

            case R.id.custom_my_app_sort_dialog_app_stat_radio_group:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1) mRadioGroup1.check(-1);
                if (mRadioGroup3.getCheckedRadioButtonId() != -1) mRadioGroup3.check(-1);
                if (mRadioGroup4.getCheckedRadioButtonId() != -1) mRadioGroup4.check(-1);

                if (checkedId == R.id.custom_my_app_sort_dialog_app_stat_radio_1)
                    mApplicationSortOrder = ApplicationsLookupSort.APPLICATION_STATUS_ASC;
                else
                    mApplicationSortOrder = ApplicationsLookupSort.APPLICATION_STATUS_DESC;
                break;

            case R.id.custom_my_app_sort_dialog_installment_radio_group:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1) mRadioGroup1.check(-1);
                if (mRadioGroup2.getCheckedRadioButtonId() != -1) mRadioGroup2.check(-1);
                if (mRadioGroup4.getCheckedRadioButtonId() != -1) mRadioGroup4.check(-1);

                if (checkedId == R.id.custom_my_app_sort_dialog_installment_radio_1)
                    mApplicationSortOrder = ApplicationsLookupSort.INSTALLMENT_VALUE_ASC;
                else
                    mApplicationSortOrder = ApplicationsLookupSort.INSTALLMENT_VALUE_DESC;
                break;

            case R.id.custom_my_app_sort_dialog_app_radio_group:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1) mRadioGroup1.check(-1);
                if (mRadioGroup2.getCheckedRadioButtonId() != -1) mRadioGroup2.check(-1);
                if (mRadioGroup3.getCheckedRadioButtonId() != -1) mRadioGroup3.check(-1);

                if (checkedId == R.id.custom_my_app_sort_dialog_app_radio_1)
                    mApplicationSortOrder = ApplicationsLookupSort.LATEST_APPLICATION_ASC;
                else
                    mApplicationSortOrder = ApplicationsLookupSort.LATEST_APPLICATION_DESC;
                break;

        }
    }

    /**
     * set popup action listener
     ***/
    public void setPopupActionListener(MyApplicationsSortPopup.ApplicationSortPopupInterface listener) {
        mActionListener = listener;
    }


    /**
     * customer sorting popup action listener interface
     ***/
    public interface ApplicationSortPopupInterface {
        void onSortSelected(ApplicationsLookupSort sorting);
    }

}
