package com.redapplemart.rukula.popup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.redapplemart.rukula.R;
import com.redapplemart.rukula.consts.ApplicationsLookupSort;
import com.redapplemart.rukula.consts.TransactionsLookupSort;

public class TransactionsSortPopup extends Dialog implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TransactionsSortPopup.TransactionsSortPopupInterface mActionListener;
    private TransactionsLookupSort mApplicationSortOrder = TransactionsLookupSort.SORT_NONE;

    private RadioGroup mRadioGroup1, mRadioGroup2, mRadioGroup3;

    public TransactionsSortPopup(@NonNull Context context) {
        super(context);
    }

    public TransactionsSortPopup(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected TransactionsSortPopup(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_transactions_sort);

        TextView cancel = findViewById(R.id.custom_trans_sort_dialog_close_textview);
        TextView ok = findViewById(R.id.custom_trans_sort_dialog_ok_textview);
        cancel.setOnClickListener(this);
        ok.setOnClickListener(this);

        mRadioGroup1 = findViewById(R.id.custom_trans_sort_dialog_name_radio_group);
        mRadioGroup2 = findViewById(R.id.custom_trans_sort_dialog_nic_radio_group);
        mRadioGroup3 = findViewById(R.id.custom_trans_sort_dialog_date_radio_group);
        mRadioGroup1.setOnCheckedChangeListener(this);
        mRadioGroup2.setOnCheckedChangeListener(this);
        mRadioGroup3.setOnCheckedChangeListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.custom_trans_sort_dialog_close_textview:
                dismiss();
                break;

            case R.id.custom_trans_sort_dialog_ok_textview:

                if (mActionListener != null)
                    mActionListener.onSortSelected(mApplicationSortOrder);
                dismiss();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.custom_trans_sort_dialog_name_radio_group:
                if (mRadioGroup2.getCheckedRadioButtonId() != -1) mRadioGroup2.check(-1);
                if (mRadioGroup3.getCheckedRadioButtonId() != -1) mRadioGroup3.check(-1);

                if (checkedId == R.id.custom_trans_sort_dialog_name_radio_1)
                    mApplicationSortOrder = TransactionsLookupSort.CUSTOMER_NAME_ASC;
                else
                    mApplicationSortOrder = TransactionsLookupSort.CUSTOMER_NAME_DESC;

                break;
            case R.id.custom_trans_sort_dialog_nic_radio_group:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1) mRadioGroup1.check(-1);
                if (mRadioGroup3.getCheckedRadioButtonId() != -1) mRadioGroup3.check(-1);

                if (checkedId == R.id.custom_trans_sort_dialog_nic_radio_1)
                    mApplicationSortOrder = TransactionsLookupSort.NIC_ASC;
                else
                    mApplicationSortOrder = TransactionsLookupSort.NIC_DESC;
                break;
            case R.id.custom_trans_sort_dialog_date_radio_group:
                if (mRadioGroup1.getCheckedRadioButtonId() != -1) mRadioGroup1.check(-1);
                if (mRadioGroup2.getCheckedRadioButtonId() != -1) mRadioGroup2.check(-1);

                if (checkedId == R.id.custom_trans_sort_dialog_date_radio_1)
                    mApplicationSortOrder = TransactionsLookupSort.PAY_DATE_ASC;
                else
                    mApplicationSortOrder = TransactionsLookupSort.PAY_DATE_DESC;
                break;
        }
    }

    /**
     * set popup action listener
     ***/
    public void setPopupActionListener(TransactionsSortPopup.TransactionsSortPopupInterface listener) {
        mActionListener = listener;
    }


    /**
     * customer sorting popup action listener interface
     ***/
    public interface TransactionsSortPopupInterface {
        void onSortSelected(TransactionsLookupSort sorting);
    }

}
