package com.redapplemart.rukula.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.redapplemart.rukula.consts.Constants;

/**
 * Created by SYSDEK Pvt Ltd on 7/31/17.
 */

public class SessionManager {

    private static SessionManager mSessionManager;
    //shared preferences object
    private SharedPreferences mSharedPreferences;
    //shared preferences editor object
    private SharedPreferences.Editor mSharedPreferencesEditor;

    private Context mContext;

    /**
     * return the singleton representation of this class
     *
     * @param context current app context
     **/
    public static SessionManager getInstance(Context context) {

        if (mSessionManager == null)
            mSessionManager = new SessionManager(context);

        return mSessionManager;
    }

    private SessionManager(Context context) {

        mContext = context;

        mSharedPreferences = context.getSharedPreferences(Constants.CUSTOM_APPLICATION_UID_SHARED_PREFERENCES
                , Context.MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();
        mSharedPreferencesEditor.apply();
    }

    /**
     * clear session manager all data
     **/
    public void clearSessionManager() {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.clear().apply();

        mSharedPreferences = null;
        mSharedPreferencesEditor = null;
        mSessionManager = null;
    }

    /**
     * remove value belongs to the given key
     **/
    public void remove(String key) throws Exception {
        if (mSharedPreferencesEditor != null) {
            mSharedPreferencesEditor.remove(key);
            mSharedPreferencesEditor.commit();
        }
    }

    /**
     * check is login details are valid or not
     **/
    public boolean isLoginSessionValid() {

        try {
            return (!Utility.isEmpty(this.getCustomerToken()) && !Utility.isEmpty(this.getMerchantID()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    /**
     * check is bank registration details are valid or not
     **/
    public boolean isBankRegistrationValid() {

        try {
            return (!Utility.isEmpty(this.getJustPayAccountReference()) && !Utility.isEmpty(this.getJustPayCustomerToken())
                    && !Utility.isEmpty(this.getUserChallengePassword()) && !Utility.isEmpty(this.getCustomerAccNo())
                    && !Utility.isEmpty(this.getCustomerAccNo()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    /**
     * save customer's account no details to shared preferences
     *
     * @param acc_no customer's account no reference
     **/
    public void saveCustomerAccountNo(String acc_no) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_ACCOUNT_NO, acc_no).apply();
    }

    /**
     * get saved customer's account no reference details
     **/
    public String getCustomerAccNo() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_ACCOUNT_NO, null);

        return null;
    }

    /**
     * save unique reference id details to shared preferences
     *
     * @param ref_id unique reference id
     **/
    public void saveUniqueRefID(String ref_id) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_REF_ID, ref_id).apply();
    }

    /**
     * get saved unique reference id details
     **/
    public String getUniqueRefID() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_REF_ID, null);

        return null;
    }

    /**
     * save user challenge password details to shared preferences
     *
     * @param challenge user challenge password
     **/
    public void saveUserChallengePassword(String challenge) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_USER_CHALLENGE, challenge).apply();
    }

    /**
     * get saved user challenge details
     **/
    public String getUserChallengePassword() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_USER_CHALLENGE, null);

        return null;
    }

    /**
     * save just pay customer's token details to shared preferences
     *
     * @param customer_token customer's token
     **/
    public void saveJustPayCustomerToken(String customer_token) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_JUST_PAY_CUSTOMER_TOKEN, customer_token).apply();
    }

    /**
     * get saved just pay customer's token details
     **/
    public String getJustPayCustomerToken() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_JUST_PAY_CUSTOMER_TOKEN, null);

        return null;
    }

    /**
     * save just pay account reference details to shared preferences
     *
     * @param account_ref customer's token
     **/
    public void saveJustPayAccountReference(String account_ref) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_JUST_PAY_ACCOUNT_REF, account_ref).apply();
    }

    /**
     * get saved just pay customer's account reference
     **/
    public String getJustPayAccountReference() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_JUST_PAY_ACCOUNT_REF, null);

        return null;
    }

    /**
     * save customer's token details to shared preferences
     *
     * @param customer_token customer's token
     **/
    public void saveCustomerToken(String customer_token) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_CUSTOMER_TOKEN, customer_token).apply();
    }

    /**
     * get saved customer's token details
     **/
    public String getCustomerToken() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_CUSTOMER_TOKEN, null);

        return null;
    }

    /**
     * save customer's merchant id details to shared preferences
     *
     * @param merchant_id customer's merchant id
     **/
    public void saveMerchantID(String merchant_id) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_MERCHANT_ID, merchant_id).apply();
    }

    /**
     * get saved customer's merchant id details
     **/
    public String getMerchantID() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_MERCHANT_ID, null);

        return null;
    }

    /**
     * save customer's PIN details to shared preferences
     *
     * @param pin customer's new PIN
     **/
    public void savePIN(String pin) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_PIN, pin).apply();
    }

    /**
     * get saved customer's PIN details
     **/
    public String getPIN() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_PIN, null);

        return null;
    }

//    /**
//     * save customer's nic details to shared preferences
//     *
//     * @param nic customer's nic
//     **/
//    public void saveNIC(String nic) throws Exception {
//        if (mSharedPreferencesEditor != null)
//            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_CUSTOMER_NIC, nic).apply();
//    }
//
//    /**
//     * get saved customer's nic details
//     **/
//    public String getNIC() throws Exception {
//        if (mSharedPreferences != null)
//            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_CUSTOMER_NIC, null);
//
//        return null;
//    }

    /**
     * save user consent details to shared preferences
     *
     * @param consent customer consent
     **/
    public void saveCustomerConsent(String consent) throws Exception {
        if (mSharedPreferencesEditor != null)
            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_CUSTOMER_CONSENT, consent).apply();
    }

    /**
     * get saved customer consent details
     **/
    public String getCustomerConsent() throws Exception {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_CUSTOMER_CONSENT, null);

        return null;
    }

//    /**
//     * save user registered bank accounts details to shared preferences
//     *
//     * @param accounts customer accounts
//     **/
//    public void saveCustomerAccounts(String accounts) throws Exception {
//        if (mSharedPreferencesEditor != null)
//            mSharedPreferencesEditor.putString(Constants.SHARED_PREFERENCES_CUSTOMER_CONSENT, accounts).apply();
//    }
//
//    /**
//     * get saved customer consent details
//     **/
//    public String getCustomerConsent() throws Exception {
//        if (mSharedPreferences != null)
//            return mSharedPreferences.getString(Constants.SHARED_PREFERENCES_CUSTOMER_CONSENT, null);
//
//        return null;
//    }

}
