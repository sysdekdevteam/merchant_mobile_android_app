package com.redapplemart.rukula.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class Utility {

    public static boolean isEmpty(String data) {

        return (data == null || data.isEmpty());

    }

    /**
     * generate random value between given limits
     **/
    public static int getRandomValue(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min) + min;
    }


    /**
     * create a progress dialog
     **/
    public static ProgressDialog createProgressDialog(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage("please wait...");
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    /**
     * get current date/time string for given pattern
     **/
    public static String getCurrentDate(String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Date date = new Date();
        return formatter.format(date);
    }

    /***load images with picasso**/
    public static void loadImage(Context context, ImageView imageView, int drawable) {
        try {
            Picasso.with(context)
                    .load(drawable)
//                .centerInside()
                    .into(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
